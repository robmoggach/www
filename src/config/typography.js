import Typography from 'typography'

// This website uses the system font stack after the placed "Lora" font
// The scaleRatio will be overwritten for smaller breakpoints in src/components/Layout
const fonts = {
    serif: [
        // 'Source Sans Pro',
        // 'Nunito',
        // 'Fira Sans',
        // 'Rufina',
        'Aleo',
        'Georgia',
        'Times',
        'Times New Roman',
        'MS Serif',
        'New York',
        'Palatino',
        'Garamond',
        'serif',
    ],
    sans: [
        // 'Fira Sans',
        // 'Open Sans',
        'Nunito',
        '-apple-system',
        'BlinkMacSystemFont',
        'Segoe UI',
        'Helvetica',
        'Arial',
        'sans-serif',
        'Apple Color Emoji',
        'Segoe UI Emoji',
        'Segoe UI Symbol',
    ],
}

const typography = new Typography({
    title: 'Moggach',
    baseFontSize: '18px',
    baseLineHeight: 1.45,
    scaleRatio: 3.157,
    headerWeight: 200,
    bodyWeight: 200,
    boldWeight: 300,
    headerFontFamily: fonts.sans,
    headerFontFamily: fonts.sans,
    bodyFontFamily: fonts.sans,
    overrideStyles: () => ({
        img: {
            marginBottom: 0,
        },
    }),
})

export const { rhythm, scale } = typography
export default typography
