module.exports = {
    url: 'https://robertmoggach.com', // Domain of your site. No trailing slash!
    pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"
    aws_cdn_prefix: 'https://cdn.moggach.com',
    googleAnalyticsID: 'UA-3781298-1',
    siteMetadata: {
        title: 'Robert Moggach', // Navigation and Site Title
        titleAlt: 'Moggach', // Title for JSONLD
        shortName: 'Moggach', // shortname for manifest. MUST be shorter than 12 characters
        description: 'Artist, Producer, Director, VFX Supervisor, Creative Director, & Entrepreneur Robert Moggach.',
        url: 'https://robertmoggach.com', // For gatsby-plugin-sitemap
        siteUrl: 'https://robertmoggach.com', // For gatsby-plugin-sitemap
        siteLanguage: 'en', // Language Tag on <html> element
        logo: './static/icons/favicon.png', // Used for SEO
        banner: './static/banner.jpg', // Used for SEO
        author: 'Robert Moggach <rob@moggach.com>', // Author for schemaORGJSONLD
        copyright: 'Copyright © 2019 Robert Moggach. All Rights Reserved.',
        keywords:
            'visual effects, vfx supervisor, moggach, robert moggach, flame, cgi, animation, design, creative director, creative, art',
        pathPrefix: '/',
        headline: 'Creative.', // Headline for schema.org JSONLD
        ogLanguage: 'en_US', // Facebook Language
        lang: 'en', // Language Tag on <html> element
        locale: 'en-gb',
        author: 'Robert Moggach <rob@moggach.com>', // Author for schemaORGJSONLD
        twitter: '@robertmoggach', // Twitter Username
        facebook: 'robmoggach', // Facebook Site Name
        social: [
            {
                name: 'twitter',
                label: 'Twitter',
                id: '@robertmoggach',
                url: 'https://twitter.com/robertmoggach',
            },
            {
                name: 'instagram',
                label: 'Instagram',
                id: '@robertmoggach',
                url: 'https://www.instagram.com/robertmoggach',
            },
            {
                name: 'github',
                label: 'GitHub',
                id: '@robmoggach',
                url: 'https://github.com/robmoggach',
            },
        ],
    },

    // JSONLD / Manifest
    favicon: './static/icons/favicon.png', // Used for manifest favicon generation
    backgroundColor: '#d99f8c',
    themeColor: '#d99f8c',
    faviconOptions: {
        logo: './static/icons/favicon.png',
        // WebApp Manifest Configuration
        appName: null, // Inferred with your package.json
        appDescription: null,
        developerName: null,
        developerURL: null,
        dir: 'auto',
        lang: 'en-US',
        background: '#d99f8c',
        theme_color: '#d99f8c',
        display: 'standalone',
        orientation: 'any',
        start_url: '/?homescreen=1',
        version: '1.0',
        icons: {
            android: true,
            appleIcon: true,
            appleStartup: true,
            coast: false,
            favicons: true,
            firefox: true,
            yandex: false,
            windows: false,
        },
    },
}
