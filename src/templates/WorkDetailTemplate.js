import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import { WorkDetail } from 'components/Work'
import { MenuSetter } from 'components/Header'

const WorkDetailTemplate = ({ data, pageContext }) => {
    return (
        <React.Fragment>
            <MenuSetter source='work' />
            <WorkDetail post={data.post} context={pageContext} />
        </React.Fragment>
    )
}

export default WorkDetailTemplate

export const pageQuery = graphql`
    query WorkDetail($slug: String!) {
        post: markdownRemark(fields: { slug: { eq: $slug } }) {
            id
            html
            fields {
                slug
                sourceName
            }
            frontmatter {
                title
                subtitle
                categories
                tags
                date
                videos {
                    caption
                    cover
                    path
                    slug
                    sources {
                        filename
                        format
                        height
                        width
                    }
                    title
                    provider
                    videoId
                }
                albums {
                    images {
                        caption
                        filename
                    }
                    path
                    slug
                    title
                }
                categories
                credits {
                    companyName
                    companySlug
                    personName
                    personSlug
                    roleSlug
                    roleTitle
                    visible
                }
            }
        }
    }
`
