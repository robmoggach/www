import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import { BlogDetail } from 'components/Blog'
import { MenuSetter } from 'components/Header'

const BlogDetailTemplate = ({ data, pageContext }) => {
    const {
        post: {
            frontmatter: { title, date, tags, categories },
            excerpt,
            id,
            html,
            fields: { slug, sourceName },
        },
    } = data
    const { next, prev } = pageContext
    return (
        <React.Fragment>
            <MenuSetter source='blog' />
            <BlogDetail post={data.post} context={pageContext} />
        </React.Fragment>
    )
}

export default BlogDetailTemplate

export const pageQuery = graphql`
    query BlogDetail($slug: String!) {
        post: markdownRemark(fields: { slug: { eq: $slug } }) {
            id
            html
            fields {
                slug
                sourceName
            }
            frontmatter {
                title
                tags
                categories
                date
                cover {
                    childImageSharp {
                        fluid(maxWidth: 1920, maxHeight: 1440, quality: 90) {
                            ...GatsbyImageSharpFluid
                        }
                    }
                }
            }
        }
    }
`
