/** @jsx jsx */
import { jsx, Box, Styled } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { ContextProvider } from 'components/Context'
import { Header } from 'components/Header'
import { BurgerMenu } from 'components/Burger'
import { MainContainer } from 'components/Layout'
import GlobalStyle from 'components/GlobalStyle'
import { BackgroundContainer } from 'components/Background'
import StateDebug from 'components/StateDebug'
import theme from 'theme'

const Wrapper = props => {
    const { children, position, zIndex } = props
    return (
        <Box
            className='wrapper'
            sx={{
                width: '100%',
                minWidth: '400px',
                height: 'auto',
                position: position,
                top: 0,
                left: 0,
                zIndex: zIndex,
                m: 0,
                p: 0,
            }}>
            {children}
        </Box>
    )
}

Wrapper.propTypes = {
    position: PropTypes.string,
    zIndex: PropTypes.string,
}

Wrapper.defaultProps = {
    position: 'relative',
    zIndex: 'auto',
}

const Layout = ({ children, path, pageContext }) => {
    switch (pageContext.layout) {
        case 'test': {
            return (
                <ContextProvider path={path}>
                    <GlobalStyle />
                    {children}
                </ContextProvider>
            )
            break
        }
        case 'old': {
            return (
                <ContextProvider path={path}>
                    <GlobalStyle />
                    <Wrapper>
                        <Header />
                        <MainContainer>{children}</MainContainer>
                    </Wrapper>
                    <BurgerMenu />
                </ContextProvider>
            )
            break
        }
        default: {
            return (
                <ContextProvider path={path}>
                    <GlobalStyle />
                    <Wrapper zIndex='header'>
                        <Header />
                    </Wrapper>

                    <BackgroundContainer />

                    <Wrapper zIndex='content'>
                        <MainContainer>{children}</MainContainer>
                    </Wrapper>

                    <BackgroundContainer clipped zIndex='mg' />

                    <BurgerMenu />
                </ContextProvider>
            )
            break
        }
    }
}

export { Layout, Wrapper }
