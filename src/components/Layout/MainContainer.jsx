/** @jsx jsx */
import { jsx, Box } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { ContextConsumer } from 'components/Context'

const Pusher = props => {
    const { children, paddingtop } = props
    return (
        <React.Fragment>
            <ContextConsumer>
                {({ data, set }) => (
                    <Box
                        sx={{
                            pt: [
                                `${data.menuOpen ? data.menuHeight + 'px' : paddingtop[0] + 'vmin'}`,
                                `${data.menuOpen ? data.menuHeight + 'px' : paddingtop[1] + 'vmin'}`,
                                // `${data.menuOpen ? paddingtop[0] + 5 : paddingtop[0]}vmin`,
                                // `${data.menuOpen ? paddingtop[1] + 5 : paddingtop[1]}vmin`,
                                `${paddingtop[2]}rem`,
                                `${paddingtop[3]}rem`,
                                `${paddingtop[4]}rem`,
                            ],
                            transition: 'padding 0.15s linear',
                        }}>
                        {children}
                    </Box>
                )}
            </ContextConsumer>
        </React.Fragment>
    )
}
Pusher.propTypes = {
    margintop: PropTypes.array,
}

Pusher.defaultProps = {
    paddingtop: [0, 0, 0, 0, 0],
}

const MainContainer = ({ children }) => (
    <React.Fragment>
        <main sx={{ variant: 'layout.main' }}>
            <Pusher>
                <Box
                    sx={{
                        maxWidth: 2880,
                        px: [0, 0, '5vmin'],
                    }}>
                    {children}
                </Box>
            </Pusher>
        </main>
    </React.Fragment>
)

export default MainContainer
