/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import { darken } from '@theme-ui/color'
import React from 'react'
import PropTypes from 'prop-types'

import { Link, navigate } from 'gatsby'
import Moment from 'react-moment'
import Picture from 'gatsby-image'
import { InfoTitle } from 'components/InfoTitle'
import { CategoryList } from 'components/Blog'

const animation = {
    transition: 'opacity 0.3s ease-in, color 0.3s ease-in',
}

class BlogListItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hover: false,
        }
    }

    toggleHover = () => {
        this.setState(state => {
            return {
                hover: !state.hover,
            }
        })
    }

    render() {
        const {
            id,
            excerpt,
            fields: { slug, sourceName },
            frontmatter: { categories, tags, date, title, cover },
        } = this.props.post
        // console.log('BlogListItem', this.props)

        return (
            <Styled.li
                sx={{
                    width: '100%',
                    display: 'flex',
                    flex: '1 1 auto',
                    mx: 0,
                    mb: '3rem',
                    p: 0,
                    img: {
                        ':hover': {
                            cursor: 'pointer',
                        },
                    },
                }}
                onMouseEnter={this.toggleHover}
                onMouseLeave={this.toggleHover}>
                <div
                    sx={{
                        px: [0, 0, 1, 2],
                        py: [1, 1, 1, 2],
                    }}>
                    <Box
                        onClick={() => navigate(slug)}
                        sx={{
                            m: '1vmax',
                            boxShadow: 'xl',
                            flexFlow: 'column nowrap',
                            borderRadius: '1vmax',
                            overflow: 'hidden',
                        }}>
                        <Picture
                            fluid={cover.childImageSharp.fluid}
                            sx={{
                                opacity: `${this.state.hover ? 1 : 0.7}`,
                                transition: animation.transition,
                                borderRadius: '2px',
                            }}
                        />
                    </Box>
                    <h1
                        sx={{
                            m: 0,
                            mt: 4,
                            mb: 1,
                            lineHeight: 1.2,
                            letterSpacing: `${this.state.hover ? 0 : -1}px`,
                            transition: animation.transition + ',letter-spacing 0.3s ease',
                            textAlign: 'center',
                        }}>
                        <Link to={slug} sx={{ variant: 'styles.a', transition: animation.transition }}>
                            {title}
                        </Link>
                    </h1>
                    <InfoTitle categories={categories} date={date} source='blog' center />

                    <p
                        sx={{
                            opacity: `${this.state.hover ? 1 : 0.75}`,
                            fontSize: '90%',
                            color: 'highlight',
                            mt: 0,
                            flex: '1 0 auto',
                            transition: animation.transition,
                        }}>
                        {excerpt}
                    </p>
                </div>
            </Styled.li>
        )
    }
}

BlogListItem.propTypes = {
    post: PropTypes.object,
}

export default BlogListItem
