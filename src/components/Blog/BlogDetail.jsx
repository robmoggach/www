/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'

import React from 'react'
import PropTypes from 'prop-types'
import Moment from 'react-moment'
import { Link } from 'gatsby'

import { CategoryList } from 'components/Blog'
import animation from 'theme'
import { NextPrev } from 'components/NextPrev'
import { InfoTitle } from 'components/InfoTitle'
import { TagList } from 'components/Collections'

const BlogDetail = props => {
    const { post, context } = props
    const {
        html,
        frontmatter: { categories, date, tags, title },
    } = post
    const tagsCount = tags.length
    const { next, prev } = context
    // console.log('BlogDetail', context)
    return (
        <Box sx={{ variant: 'styles.spacer.detail' }}>
            <Styled.h1 sx={{ color: 'primary', textAlign: 'center' }}>{title}</Styled.h1>
            <InfoTitle categories={categories} date={date} source='blog' center />

            <article sx={{ mt: 4 }} dangerouslySetInnerHTML={{ __html: html }} />

            {tags.length != 0 && <h3 sx={{ textAlign: 'center', mt: 5 }}>Tags</h3>}

            {tags.length != 0 && <TagList sourceName='blog' tags={tags} />}
            <NextPrev
                prev={next ? next.fields.slug : ''}
                next={prev ? prev.fields.slug : ''}
                nextText={prev ? '\u00ab ' + (prev.frontmatter.title || '') : 'Newer'}
                prevText={(next ? next.frontmatter.title || ' ' : 'Older') + ' \u00bb'}
            />
        </Box>
    )
}

BlogDetail.propTypes = {
    post: PropTypes.object,
}

export default BlogDetail
