/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { rotate } from '@theme-ui/color'
import { visible } from 'ansi-colors'
import { animation } from 'theme'

const BurgerLine = props => {
    const { menuopen, width, height, lineIndex, ratio, size, strokeWidth, speed } = props
    var sw = strokeWidth
    var swOff = ~~(strokeWidth / 2)
    var sizeScaled = size.map(y => ~~(y * ratio * 0.5))
    var translator =
        lineIndex == 1
            ? sizeScaled.map(
                  y => 'translateY(' + (menuopen ? ~~(-swOff).toString() : ~~(-y - swOff).toString()) + 'px)'
              )
            : sizeScaled.map(y => 'translateY(' + (menuopen ? ~~(-swOff).toString() : ~~(y - swOff).toString()) + 'px)')
    var rotator =
        lineIndex == 1
            ? sizeScaled.map(y => 'rotate(' + (menuopen ? 45 : 0) + 'deg)')
            : sizeScaled.map(y => 'rotate(' + (menuopen ? -45 : 0) + 'deg)')
    return (
        <Box
            sx={{
                overflow: 'visible',
                display: 'block',
                position: 'absolute',
                width: '100%',
                height: sw,
                borderRadius: swOff,
                transformOrigin: 'center',
                height: `${sw}px`,
                overflow: 'visible',
                transform: translator,
                transition: `${
                    menuopen
                        ? `${`transform ${animation.quick.ms}ms cubic-bezier(.8,1.27,.73,.26)`}`
                        : `${`transform ${animation.quick.ms}ms ${animation.quick.ms *
                              2}ms cubic-bezier(.8,1.27,.73,.26)`}`
                }`,
            }}>
            <Box
                className='BurgerLine'
                sx={{
                    overflow: 'visible',
                    display: 'block',
                    position: 'absolute',
                    width: '100%',
                    height: sw,
                    backgroundColor: 'primary',
                    borderRadius: swOff,
                    transformOrigin: 'center',
                    height: `${sw}px`,
                    borderRadius: `${swOff}px`,
                    overflow: 'visible',
                    transform: rotator,
                    transition: `background-color 0.3s, ${
                        menuopen
                            ? `${`transform ${animation.quick.ms}ms ${animation.quick.ms *
                                  2}ms cubic-bezier(.38,.46,.99,.46)`}`
                            : `${`transform ${animation.quick.ms}ms cubic-bezier(.38,.46,.99,.46)`}`
                    }`,
                }}
            />
        </Box>
    )
}
BurgerLine.propTypes = {
    ratio: PropTypes.number,
    menuopen: PropTypes.bool,
}

BurgerLine.defaultProps = {
    ratio: '35%',
    menuopen: false,
}

const Burger = props => {
    const { menuopen, onClick, width, height, strokeWidth, ratio, size, speed } = props
    var sizePx = size.map(y => y.toString() + 'px')
    return (
        <Box
            sx={{
                position: 'absolute',
                right: [0, 'auto'],
                bottom: 0,
                height: ['3.5rem', '3.5rem', '4rem', '5rem'],
            }}>
            <button
                sx={{
                    font: 'inherit',
                    color: 'inherit',
                    textTransform: 'none',
                    border: 'none',
                    backgroundColor: 'transparent',
                    m: 1,
                    p: 0,
                    display: 'inline-block',
                    cursor: 'pointer',
                    transition: `background-color ${speed}s, opacity ${speed}s`,
                    '&:focus': {
                        outline: 'none',
                    },
                    overflow: 'visible',
                    position: 'relative',

                    '&:hover': {
                        opacity: 1,
                        span: {
                            width: '100%',
                        },
                        '.BurgerLine': {
                            backgroundColor: 'secondary',
                        },
                    },
                    width: sizePx,
                    height: sizePx,
                    // width: '5vmin',
                    // height: '5vmin',
                }}
                onClick={onClick}>
                <BurgerLine
                    menuopen={menuopen}
                    lineIndex='1'
                    ratio={ratio}
                    size={size}
                    strokeWidth={strokeWidth}
                    speed={speed}
                />
                <BurgerLine
                    menuopen={menuopen}
                    lineIndex='2'
                    ratio={ratio}
                    size={size}
                    strokeWidth={strokeWidth}
                    speed={speed}
                />
            </button>
        </Box>
    )
}
Burger.propTypes = {
    strokeWidth: PropTypes.number,
    size: PropTypes.array,
    ratio: PropTypes.number,
    speed: PropTypes.number,
    menuopen: PropTypes.bool,
}

Burger.defaultProps = {
    strokeWidth: 4,
    size: [32, 32, 40, 50],
    ratio: 0.3,
    speed: 0.05,
    menuopen: false,
}

export default Burger
