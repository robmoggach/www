/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
import { darken } from '@theme-ui/color'
import React from 'react'
import PropTypes from 'prop-types'
import config from 'config'

// tricky using Plyr in Gatsby
// https://chaseohlson.com/plyr-gatsby-youtube-vimeo

const VideoPlayer = props => {
    // console.log('Video', props)
    const { provider, cover, path, videoId, url, sources } = props.video

    React.useEffect(() => {
        if (typeof window !== 'undefined' && typeof document !== 'undefined') {
            const Plyr = require('plyr')
            Array.from(document.querySelectorAll('.js-player')).map(p => new Plyr(p))
        }
    }, [])

    switch (provider) {
        case 'html5': {
            return sources && Array.isArray(sources) && sources.length ? (
                <React.Fragment>
                    <video
                        poster={cover && config.aws_cdn_prefix + '/' + cover}
                        data-plyr-provider={provider}
                        className='js-player'
                        controls
                        playsInline
                        preload='none'>
                        {sources.map((source, i) => (
                            <source
                                key={i}
                                src={config.aws_cdn_prefix + '/' + path + '/' + source.filename}
                                type={`video/${source.format}`}
                                size={source.size && source.size}
                            />
                        ))}
                    </video>
                </React.Fragment>
            ) : (
                <React.Fragment>
                    <video
                        src={url}
                        poster={cover && config.aws_cdn_prefix + '/' + cover}
                        className='js-player'
                        controls
                        playsinline
                        preload='none'></video>
                </React.Fragment>
            )
            break
        }
        default: {
            return (
                <React.Fragment>
                    <div className='js-player' data-plyr-provider={provider} data-plyr-embed-id={videoId} />
                </React.Fragment>
            )
        }
    }
}

VideoPlayer.propTypes = {
    video: PropTypes.shape({
        provider: PropTypes.oneOf(['youtube', 'vimeo', 'html5', 'audio']).isRequired,
        poster: PropTypes.string,
        videoId: PropTypes.string,
        path: PropTypes.string,
        sources: PropTypes.arrayOf(
            PropTypes.shape({
                filename: PropTypes.string.isRequired,
                format: PropTypes.string.isRequired,
                size: PropTypes.number,
                width: PropTypes.string,
                height: PropTypes.string,
            })
        ),
    }),
}

// videos:
//     - caption: ''
//       path: /work/2008/dlp-color-musings/videos/dlp-color-musings
//       cover: /work/2008/dlp-color-musings/videos/dlp-color-musings.jpg
//       slug: dlp-color-musings
//       sources:
//           - filename: dlp-color-musings-1280x720.mp4
//             format: mp4
//             height: '720'
//             size: 11578734
//             width: '1280'

VideoPlayer.defaultProps = {
    video: {
        provider: 'html5',
        poster: null,
        videoId: null,
        path: null,
        sources: [],
    },
}

const Video = props => {
    const { title, caption, video } = props

    return (
        <Box
            sx={{
                my: 3,
                '.plyr': {
                    width: '100%',
                    height: '100%',
                    borderRadius: '0.5rem',
                    boxShadow: 'xl',
                    '.plyr__video-wrapper': {
                        background: 'none',
                    },

                    'input[type=range]': {
                        color: 'secondary',
                    },
                    '.plyr__control': {
                        '.plyr__control--overlaid': {
                            backgroundColor: 'secondary',
                        },
                        background: 'secondary',
                        svg: {
                            fill: 'white',
                        },
                        '&:hover': {
                            backgroundColor: 'secondary',
                            svg: {
                                fill: 'white',
                            },
                        },
                    },
                    '.plyr__controls button': {
                        backgroundColor: 'highlight',

                        '&:hover': {
                            backgroundColor: 'secondary',
                        },
                    },
                    'button.plyr__control.plyr__control--overlaid': {
                        backgroundColor: 'highlight',
                        '&:hover': {
                            backgroundColor: 'secondary',
                        },
                    },
                },
            }}>
            {title ? <Styled.h3>{title}</Styled.h3> : null}
            <VideoPlayer video={video} />
            {caption ? <Styled.h5>{caption}</Styled.h5> : null}
        </Box>
    )
}

Video.propTypes = {
    title: PropTypes.string,
    caption: PropTypes.string,
    video: PropTypes.object,
}

Video.defaultProps = {
    title: null,
    caption: null,
    video: {},
}

export default Video
