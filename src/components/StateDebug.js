/** @jsx jsx */
import { jsx, Box } from 'theme-ui'
import React from 'react'
import { ContextConsumer } from 'components/Context'

const StateDebug = props => {
    return (
        <React.Fragment>
            <ContextConsumer>
                {({ data, set }) => {
                    // console.log('StateDebug', data)
                    return (
                        <Box
                            sx={{
                                backgroundColor: 'backgroundDarker',
                                padding: 2,
                                opacity: 0.5,
                                fontSize: '10px',
                                position: 'fixed',
                                top: '0',
                                right: '10%',
                                zIndex: 20000,
                            }}>
                            menuOpen: {data.menuOpen ? 'TRUE' : 'FALSE'}
                            <br />
                            burgerOpen: {data.burgerOpen ? 'TRUE' : 'FALSE'}
                            <br />
                            searchOpen: {data.searchOpen ? 'TRUE' : 'FALSE'}
                            <br />
                            searchOpen: {data.searchOpen ? 'TRUE' : 'FALSE'}
                            <br />
                            menu.source: {data.menu.source}
                            <br />
                            menu.open: {data.menu.open ? 'TRUE' : 'FALSE'}
                            <br />
                            menuItems ({data.menu.items.length}):{' '}
                            {data.menu.items.map((item, i) => (
                                <React.Fragment key={i}>{item.label} </React.Fragment>
                            ))}
                            <br />
                            background: {data.background ? 'TRUE' : 'FALSE'}
                        </Box>
                    )
                }}
            </ContextConsumer>
        </React.Fragment>
    )
}

export default StateDebug
