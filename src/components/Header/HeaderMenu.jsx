/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React, { useLayoutEffect, useState, useContext } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import AnimateHeight from 'react-animate-height'
import { ContextConsumer } from 'components/Context'
import { GlobalContext } from '../../context'

const MenuItem = props => {
    return (
        <li
            sx={{
                mx: ['0.5rem', '1rem', 0],
                p: 0,
                lineHeight: ['2rem', '2rem', '3rem', '3.5rem', '4rem'],
                flex: '0 0 auto',
                ':hover': {},
                '.active': {
                    opacity: '1',
                },
            }}>
            <Link sx={{ variant: 'styles.menu' }} activeClassName='active' {...props} />
        </li>
    )
}

class HeaderMenu extends React.Component {
    static contextType = GlobalContext
    constructor(props) {
        super(props)
        const defaultHeight = 0
        this.state = {
            menuHeight: defaultHeight,
        }
        this.menuRef = React.createRef()
        this.getRectsInterval = undefined
    }
    componentDidUpdate() {
        const { data, set } = this.props
        console.log('data', data)
        // setTimeout(() => {
        const menuHeight = ~~this.menuRef.current.getBoundingClientRect()['height'].toFixed(2)
        console.log('didmount', menuHeight)
        if (data.menuHeight != menuHeight) {
            set({ menuHeight: menuHeight })
        }
        // }, 200)
    }
    componentDidMount() {
        // console.log(this.context)
        // const self = this
        // const menuHeight = ~~this.menuRef.current.getBoundingClientRect()['height'].toFixed(2)
        // console.log('didmount', menuHeight)
        // self.context.set({ menuHeight: menuHeight })
        // this.getRectsInterval = setInterval(() => {
        //     this.setState(state => {
        //         const menuHeight = ~~this.menuRef.current.getBoundingClientRect()['height'].toFixed(2)
        //         console.log('setInterval', menuHeight)
        //         // console.log(this.context.data.menuHeight)
        //         if (menuHeight != self.context.data.menuHeight) {
        //             self.context.set({ menuHeight: menuHeight })
        //             console.log(self.context.data)
        //         }
        //         // console.log(this.context)
        //         return JSON.stringify(menuHeight) !== JSON.stringify(state.menuHeight) ? null : { menuHeight }
        //     })
        // }, 300)
        // this.menuRef.addEventListener()
    }

    componentWillUnmount() {
        // self.context.set({ menuHeight: 0 })
        // clearInterval(this.getRectsInterval)
    }

    render() {
        const { menuOpen, items } = this.props
        const width = this.state.menuHeight
        const height = this.state.menuHeight
        return (
            <ContextConsumer>
                {({ data, set }) => {
                    console.log('render', data)
                    return (
                        <React.Fragment>
                            {/* <Box
                                sx={{
                                    backgroundColor: 'red',
                                    width: `${data.menuHeight + 10}px`,
                                    height: `${data.menuHeight + 10}px`,
                                    display: 'fixed',
                                    top: 0,
                                    left: 0,
                                    zIndex: 100000,
                                }}
                            /> */}
                            <nav
                                sx={{
                                    width: '100%',
                                    transition: 'width 0.2s',
                                    height: 'auto',
                                    display: 'flex',
                                    flexFlow: ['row wrap', 'row wrap', 'column nowrap'],
                                    alignItems: 'flex-end',
                                    justifyContent: ['center', 'center', 'flex-start'],
                                }}>
                                <AnimateHeight duration={150} height={`${menuOpen ? 'auto' : '0%'}`}>
                                    <div
                                        sx={{
                                            position: 'relative',
                                            transform: `translateY(${menuOpen ? '0' : '-10%'})`,
                                            zIndex: 9000,
                                            opacity: `${menuOpen ? '1' : '0'}`,
                                            transition: 'transform 0.2s ease-in-out, opacity 0.2s 0.1s linear',
                                        }}
                                        ref={this.menuRef}>
                                        <ul
                                            sx={{
                                                m: 0,
                                                p: 0,
                                                width: '100%',
                                                listStyleType: 'none',
                                                display: 'flex',
                                                flexFlow: ['row wrap', 'row wrap', 'column wrap'],
                                                justifyContent: 'center',
                                                alignItems: 'flex-end',
                                            }}>
                                            {items.map((item, i) => (
                                                <MenuItem key={i} to={item.to}>
                                                    {item.label}
                                                </MenuItem>
                                            ))}
                                        </ul>
                                    </div>
                                </AnimateHeight>
                            </nav>
                        </React.Fragment>
                    )
                }}
            </ContextConsumer>
        )
    }
}

HeaderMenu.propTypes = {
    source: PropTypes.string,
    items: PropTypes.array,
    open: PropTypes.bool,
}

HeaderMenu.defaultProps = {
    source: null,
    open: false,
    items: [],
}

export default HeaderMenu
