import {
    Logo,
    Title,
    Subtitle,
    MenuItem,
    Heading1,
    Heading2,
    Heading3,
    Heading4,
    Heading5,
    Body,
    Blockquote,
} from './Elements'

export { Logo, Title, Subtitle, MenuItem, Heading1, Heading2, Heading3, Heading4, Heading5, Body, Blockquote }
