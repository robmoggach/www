/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import { MenuSetter } from 'components/Header'
import { CategoryList } from 'components/Collections'

const CategoryListPage = props => {
    const { categories, source } = props
    return (
        <Box sx={{ position: 'relative', mt: ['27vmin', '30vmin', '31vmin', '31vmin', '30vmin'] }}>
            {source != 'all' ? (
                <React.Fragment>
                    <MenuSetter source={source} />
                    <Styled.h1 sx={{ textAlign: ['center', 'center', 'left'], mb: [2, 2, 4] }}>Categories</Styled.h1>
                    <CategoryList source={source} categories={categories} />
                </React.Fragment>
            ) : (
                <React.Fragment>
                    <MenuSetter />
                    <Styled.h1 sx={{ textAlign: ['center', 'center', 'left'], mb: [2, 2, 4] }}>Categories</Styled.h1>
                    <CategoryList source={source} categories={categories} />
                </React.Fragment>
            )}
        </Box>
    )
}

CategoryListPage.propTypes = {
    categories: PropTypes.arrayOf(PropTypes.string),
    source: PropTypes.string,
}

CategoryListPage.defaultProps = {
    categories: [],
    source: '',
}
export default CategoryListPage
