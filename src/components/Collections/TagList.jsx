/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'

import React from 'react'
import PropTypes from 'prop-types'
import Moment from 'react-moment'
import { Link } from 'gatsby'
import { CategoryList } from 'components/Work'

import kebabCase from 'lodash/kebabCase'

import { get } from '@styled-system/css'
import { transparentize } from 'polished'
import theme from 'theme'
import startCase from 'lodash/startCase'
// get(theme, `colors.background`)

const TagList = props => {
    // console.log('TagList', props.tags)
    const { tags, source } = props

    return (
        <Box>
            <ul
                sx={{
                    listStyleType: 'none',
                    m: 0,
                    p: 0,
                    display: 'flex',
                    flexFlow: 'row wrap',
                    alignItems: 'center',
                    justifyContent: 'center',
                    textAlign: 'center',
                    fontSize: [1, 1, 2, 2],
                    textIndent: 0,
                }}>
                {tags.map((tag, i) => (
                    <li key={i} sx={{ flex: '0 0 auto', mx: '0.5vmax', my: '1vmax' }}>
                        <Styled.a
                            as={Link}
                            sx={{
                                px: '1vmax',
                                py: '0.5vmax',
                                backgroundColor: 'button',
                                display: 'inline',
                                color: 'buttonColor',
                                textDecoration: 'none',
                                fontWeight: 'extrabold',
                                fontSize: 1,
                                '&:hover': {
                                    backgroundColor: 'buttonHover',

                                    color: 'buttonColorHover',
                                },
                                borderRadius: '2vmax',
                                transition: 'opacity 0.3s, color 0.3s',
                            }}
                            to={(source ? source + '/' : '/') + 'tags/' + kebabCase(tag)}>
                            {`${startCase(tag)}`}
                        </Styled.a>
                    </li>
                ))}
            </ul>
        </Box>
    )
}

TagList.propTypes = {
    tags: PropTypes.arrayOf(PropTypes.string),
    source: PropTypes.string,
}

TagList.defaultProps = {
    tags: [],
    source: null,
}
export default TagList
