/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React, { useState, useEffect } from 'react'
import { StaticQuery, graphql } from 'gatsby'

import { Video } from 'components/Video'
import { animation } from 'theme'
import { Revealer } from 'components/Revealer'

const WorkHome = () => (
    <StaticQuery
        query={graphql`
            query {
                markdownRemark(fileAbsolutePath: { glob: "**/work.md" }) {
                    id
                    fields {
                        slug
                        sourceName
                        year
                    }
                    frontmatter {
                        albums {
                            path
                            slug
                            title
                        }
                        title
                        categories
                        tags
                        videos {
                            provider
                            videoId
                            caption
                            path
                            slug
                            title
                            sources {
                                format
                                width
                                height
                                filename
                            }
                        }
                        date
                    }
                    html
                }
            }
        `}
        render={data => {
            const [showidx, setShowIndex] = useState(0)

            useEffect(() => {
                const timer = showidx < 7 ? setTimeout(() => setShowIndex(showidx + 1), animation.page.ms) : null
                return () => (timer ? clearTimeout(timer) : null)
            })
            // console.log('WorkHome', data)
            const titleWords = data.markdownRemark.frontmatter.title.split(' ')
            return (
                <Box sx={{ variant: 'styles.spacer.h1' }}>
                    <Styled.h1>
                        {titleWords.map((word, i) => {
                            return (
                                <Revealer showidx={showidx} hookidx={i} key={i}>
                                    <span
                                        sx={{
                                            mr: 2,
                                        }}>
                                        {word}
                                    </span>
                                </Revealer>
                            )
                        })}
                    </Styled.h1>
                    <Revealer showidx={showidx} hookidx={3}>
                        <article dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }} />
                    </Revealer>
                    <Revealer showidx={showidx} hookidx={4} sx={{ width: '100%', height: '100%' }}>
                        {data.markdownRemark.frontmatter.videos.length &&
                            data.markdownRemark.frontmatter.videos.map((v, i) => {
                                return (
                                    <React.Fragment key={i}>
                                        <Video key={i} title={v.title} caption={v.caption} video={v} />
                                    </React.Fragment>
                                )
                            })}
                    </Revealer>
                </Box>
            )
        }}
    />
)

export default WorkHome
