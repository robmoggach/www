/** @jsx jsx */
import { jsx, useColorMode, Styled, Box } from 'theme-ui'
import React from 'react'

const modes = ['warm', 'light', 'dark']

import { get } from '@styled-system/css'
import { transparentize as Ptrans } from 'polished'
const g = (t, c) => get(t, `colors.${c}`, c)
export const transparentize = (c, n) => t => Ptrans(n, g(t, c))

const ColorMode = props => {
    const [mode, setMode] = useColorMode()

    return (
        <button
            {...props}
            onClick={e => {
                const index = modes.indexOf(mode)
                const next = modes[(index + 1) % modes.length]
                setMode(next)
            }}
            sx={{ variant: 'buttons.site', mt: 4 }}>
            {mode.toUpperCase()}
        </button>
    )
}

export default ColorMode
