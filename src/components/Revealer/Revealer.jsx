/** @jsx jsx */
import { jsx, Box } from 'theme-ui'
import React from 'react'

const Revealer = props => {
    const { children, showidx, hookidx } = props

    return (
        <Box
            showidx={showidx}
            sx={{
                opacity: `${showidx > hookidx ? '1' : '0'}`,
                transition: 'opacity 0.75s ease-in',
                display: 'inline-block',
            }}
            {...props}
        />
    )
}

export default Revealer
