/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
import React from 'react'
import { Link } from 'gatsby'

import { LoremIpsum } from 'lorem-ipsum'
const lorem = new LoremIpsum({
    sentencesPerParagraph: {
        max: 8,
        min: 4,
    },
    wordsPerSentence: {
        max: 16,
        min: 4,
    },
})

const content = {
    title: 'Testing, 1, 2, 3',
    subtitle: 'Checking out theme-ui',
    tagline: "It's only a test",
    lorem:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
}

const Welcome = props => {
    return (
        <React.Fragment>
            <Styled.h1>{content.title}</Styled.h1>
            <Styled.p>{content.lorem}</Styled.p>
            <Styled.h2>{content.tagline}</Styled.h2>
            <Styled.p>{content.lorem}</Styled.p>
            <Styled.h3>{content.subtitle}</Styled.h3>
            <Styled.h1>{content.title}</Styled.h1>
            <Styled.p>{content.lorem}</Styled.p>
            <Styled.h2>{content.tagline}</Styled.h2>
            <Styled.p>{content.lorem}</Styled.p>
        </React.Fragment>
    )
}

export default Welcome
