import React from 'react'
import Helmet from 'react-helmet'
import PropTypes from 'prop-types'
import { useSiteMetadata } from 'hooks'

const SEO = props => {
    const { title, description, banner, pathname, article, sourceName, children } = props

    const {
        buildTime,
        siteMetadata: {
            defaultTitle,
            titleAlt,
            shortName,
            author,
            siteLanguage,
            logo,
            siteUrl,
            pathPrefix,
            defaultDescription,
            defaultBanner,
            twitter,
        },
    } = useSiteMetadata()

    const seo = {
        title: title ? defaultTitle + ' | ' + title : defaultTitle,
        description: defaultDescription || description,
        image: `${siteUrl}${banner || defaultBanner}`,
        url: `${siteUrl}${pathname || '/'}`,
    }

    const realPrefix = pathPrefix === '/' ? '' : pathPrefix

    let schemaOrgJSONLD = [
        {
            '@context': 'http://schema.org',
            '@type': 'WebSite',
            '@id': siteUrl,
            url: siteUrl,
            name: defaultTitle,
            alternateName: titleAlt || '',
        },
    ]

    if (sourceName) {
        switch (sourceName) {
            case 'blog':
                {
                    schemaOrgJSONLD = [
                        {
                            '@context': 'http://schema.org',
                            '@type': 'BlogPosting',
                            '@id': seo.url,
                            url: seo.url,
                            name: title,
                            alternateName: titleAlt || '',
                            headline: title,
                            image: {
                                '@type': 'ImageObject',
                                url: seo.image,
                            },
                            description: seo.description,
                            datePublished: buildTime,
                            dateModified: buildTime,
                            author: {
                                '@type': 'Person',
                                name: author,
                            },
                            publisher: {
                                '@type': 'Organization',
                                name: author,
                                logo: {
                                    '@type': 'ImageObject',
                                    url: siteUrl + realPrefix + logo,
                                },
                            },
                            isPartOf: siteUrl,
                            mainEntityOfPage: {
                                '@type': 'WebSite',
                                '@id': siteUrl,
                            },
                        },
                    ]
                }
                break
            case 'work': {
                schemaOrgJSONLD = [
                    {
                        '@context': 'http://schema.org',
                        '@type': 'CreativeWork',
                        '@id': seo.url,
                        url: seo.url,
                        name: title,
                        alternateName: titleAlt || '',
                        headline: title,
                        image: {
                            '@type': 'ImageObject',
                            url: seo.image,
                        },
                        description: seo.description,
                        datePublished: buildTime,
                        dateModified: buildTime,
                        author: {
                            '@type': 'Person',
                            name: author,
                        },
                        publisher: {
                            '@type': 'Organization',
                            name: author,
                            logo: {
                                '@type': 'ImageObject',
                                url: siteUrl + realPrefix + logo,
                            },
                        },
                        isPartOf: siteUrl,
                        mainEntityOfPage: {
                            '@type': 'WebSite',
                            '@id': siteUrl,
                        },
                    },
                ]
            }
        }
    }

    return (
        <React.Fragment>
            <Helmet title={seo.title}>
                <html lang={siteLanguage} />
                <meta name='description' content={seo.description} />
                <meta name='image' content={seo.image} />
                <meta name='apple-mobile-web-app-title' content={shortName} />
                <meta name='application-name' content={shortName} />
                <script type='application/ld+json'>{JSON.stringify(schemaOrgJSONLD)}</script>

                {/* OpenGraph  */}
                <meta property='og:url' content={seo.url} />
                <meta property='og:type' content={article ? 'article' : null} />
                <meta property='og:title' content={seo.title} />
                <meta property='og:description' content={seo.description} />
                <meta property='og:image' content={seo.image} />

                {/* Twitter Card */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:creator' content={twitter} />
                <meta name='twitter:title' content={seo.title} />
                <meta name='twitter:description' content={seo.description} />
                <meta name='twitter:image' content={seo.image} />
            </Helmet>
        </React.Fragment>
    )
}

// <link rel="apple-touch-icon" sizes="180x180" href="/static/icons/apple-touch-icon.png">
// <link rel="icon" type="image/png" sizes="32x32" href="/static/icons/favicon-32x32.png">
// <link rel="icon" type="image/png" sizes="16x16" href="/static/icons/favicon-16x16.png">
// <link rel="manifest" href="/static/icons/site.webmanifest">
// <link rel="mask-icon" href="/static/icons/safari-pinned-tab.svg" color="#d99f8c">
// <link rel="shortcut icon" href="/static/icons/favicon.ico">
// <meta name="apple-mobile-web-app-title" content="Robert Moggach">
// <meta name="application-name" content="Robert Moggach">
// <meta name="msapplication-TileColor" content="#d99f8c">
// <meta name="msapplication-config" content="/static/icons/browserconfig.xml">
// <meta name="theme-color" content="#ffffff"></meta>

SEO.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    banner: PropTypes.string,
    pathname: PropTypes.string,
    article: PropTypes.bool,
}

SEO.defaultProps = {
    title: null,
    description: null,
    banner: null,
    pathname: null,
    article: false,
}

export default SEO
