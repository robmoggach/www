const _ = require('lodash')
const path = require(`path`)

const { paginate } = require('gatsby-awesome-pagination')

// COLLECTIONS
// Collections are:
//     tags, categories, classifications, etc.
// They are multiple ways of categorizing posts, projects, entries, etc.
// This function takes a source and creates detail and list pages per collection
// Eg.
//     createCollectionPages([data.workEdges], createPage, 'tags')
//         takes the list of markdown docs defined in workEdges and creates
//         detail and list pages for the "tags"

const createCollectionPages = (edges, createPage, collectionName, listTemplate, detailTemplate, postsPerDetailPage) => {
    const nodesByCollectionBySource = {}

    nodesByCollectionBySource.all = {}

    edges.forEach((edge, i) => {
        sourceName = edge.node.fields.sourceName
        if (!nodesByCollectionBySource[sourceName]) {
            nodesByCollectionBySource[sourceName] = {}
        }
        const collectionsArray = eval(`edge.node.frontmatter.${collectionName}`)
        if (collectionsArray) {
            collectionsArray.forEach(collection => {
                if (!nodesByCollectionBySource.all[collection]) {
                    nodesByCollectionBySource.all[collection] = []
                }
                nodesByCollectionBySource.all[collection].push(edge)
                if (!nodesByCollectionBySource[sourceName][collection]) {
                    nodesByCollectionBySource[sourceName][collection] = []
                }
                nodesByCollectionBySource[sourceName][collection].push(edge)
            })
        }
    })
    // ITERATE AND CREATE DETAIL AND LIST PAGES!!!
    Object.entries(nodesByCollectionBySource).forEach(entry => {
        const [sourceName, collectionsForSource] = entry
        // COLLECTION NAMES
        const collections = Object.keys(collectionsForSource)
        // COLLECTION LIST PAGES (one for each sourceName)
        const collectionsPath = sourceName == 'all' ? `/${collectionName}` : `/${sourceName}/${collectionName}`
        createPage({
            path: collectionsPath,
            component: listTemplate,
            context: {
                pathSlug: collectionsPath,
                collectionName,
                collections,
                sourceName,
                edges,
            },
        })
        // COLLECTION DETAIL PAGES (one for each collection for each sourceName)
        collections.map((collection, index) => {
            const edges = collectionsForSource[collection]
            if (sourceName == 'all') {
                const path = `/${collectionName}/${_.kebabCase(collection)}`
                // paginate({
                //     createPage: createPage,
                //     component: detailTemplate,
                //     items: edges,
                //     itemsPerPage: 3,
                //     pathPrefix: path,
                //     context: {
                //         pathSlug: path,
                //         collectionName,
                //         edges,
                //         collection,
                //         sourceName,
                //     },
                // })
                createPage({
                    path,
                    component: detailTemplate,
                    context: {
                        pathSlug: path,
                        collectionName,
                        edges,
                        collection,
                        sourceName,
                    },
                })
            } else {
                const template = path.resolve(`./src/templates/${sourceName}-list.js`)
                const path1 = `/${sourceName}/${collectionName}/${_.kebabCase(collection)}`
                // paginate({
                //     createPage: createPage,
                //     component: detailTemplate,
                //     items: edges,
                //     itemsPerPage: 3,
                //     pathPrefix: path1,
                //     context: {
                //         pathSlug: path1,
                //         collectionName,
                //         edges,
                //         collection,
                //         sourceName,
                //     },
                // })
                createPage({
                    path: path1,
                    component: detailTemplate,
                    context: {
                        pathSlug: path1,
                        collectionName,
                        edges,
                        collection,
                        sourceName,
                    },
                })
            }
        })
    })
}

module.exports = {
    createCollectionPages,
}
