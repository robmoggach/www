require('typeface-nunito')
require('typeface-montserrat')
require('plyr/dist/plyr.css')
require('prismjs/themes/prism-okaidia.css')

export { wrapPageElement } from './src/gatsby/wrapPageElement'

export const onClientEntry = () => {
    // IntersectionObserver polyfill for gatsby-background-image (Safari, IE)
    if (typeof window.IntersectionObserver === `undefined`) {
        import(`intersection-observer`)
        console.log(`# IntersectionObserver is polyfilled!`)
    }
}
