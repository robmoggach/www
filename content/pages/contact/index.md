---
date: '2019-07-15T05:00:00Z'
excerpt: ''
slug: contact
title: 'Hi,| This is Rob.'
email: rob@moggach.com
location: Toronto, Canada
mobile: +1 647 544 1793
chatlink: /chat/
---

I'm available for freelance, contract, and full service production inquiries.

### Let's get to work...

I'll ideate, collaborate with peers, build full productions, create studios within studios, craft, invent & innovate on demand, or simply put out all the fires. It's your call.

If it's creative and technically challenging, I definitely want to do it.

### Where I'm at...
