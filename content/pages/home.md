---
title: Home
slug: mercedes-first-is-forever
---

## "Hi, this is Rob."

I’m an artist, creative director, producer, director, vfx supervisor, compositor, designer, animator, photographer, editor, colorist, lighter, creative coder & entrepreneur.
