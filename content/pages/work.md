---
albums:
categories: []
credits: []
date: '2019-07-15T05:00:00Z'
excerpt: ''
headline: ''
cover:
slug: work-home
tags: []
title: Do The Work...
videos:
    - provider: vimeo
      videoId: '95181087'
      title: 2014 General Reel
      caption:
      path: https://player.vimeo.com/video/95181087
      poster: ''
      sources: []
---

[The Work](/work/featured) is my focus, my inspiration, and my muscle memory.
If it's creative and technically challenging,
**I want to to do it**.

I do all the things and thrive on defying expectations of what is possible.
