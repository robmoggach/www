---
categories:
- Advertising
- Feature Films
- Music Videos
- Virtual Reality
- Experiential
tags:
- VFX
- CGI
- Animation
- Compositing
- Automotive
- Characters
- Environments
- Set Extensions
- Warfare
- Crowds
- Vehicles
- Aircraft
- Tracking
- FX
- Cellular
- Projection
- 2.5D
- Photogrammetry
- Design
- AR
- VR
- Virtual Production
- On-Set Supervision
- Fashion
- Art
- Comedy
- Food
- Celebrity
- Clothing
- Colour
- Health
- Health
- Photo-Real
- Creatures
- Rig Removal
published: true
title: Work
featured: false

---
