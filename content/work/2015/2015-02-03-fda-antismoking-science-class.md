---
title: FDA Anti-Smoking | "Science Class"
subtitle: 'Science Class'
slug: fda-antismoking-science-class
date: 2015-02-03
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: fda-antismoking-science-class.01.jpg
          - caption: ''
            filename: fda-antismoking-science-class.02.jpg
          - caption: ''
            filename: fda-antismoking-science-class.03.jpg
          - caption: ''
            filename: fda-antismoking-science-class.04.jpg
          - caption: ''
            filename: fda-antismoking-science-class.05.jpg
          - caption: ''
            filename: fda-antismoking-science-class.06.jpg
          - caption: ''
            filename: fda-antismoking-science-class.07.jpg
          - caption: ''
            filename: fda-antismoking-science-class.08.jpg
          - caption: ''
            filename: fda-antismoking-science-class.09.jpg
      path: work/2015/fda-antismoking-science-class/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/fda-antismoking-science-class/videos/fda-antismoking-science-class.jpg
      path: work/2015/fda-antismoking-science-class/videos/fda-antismoking-science-class
      provider: html5
      slug: fda-antismoking-science-class
      sources:
          - filename: fda-antismoking-science-class-1280x720.mp4
            format: mp4
            height: '720'
            size: 5463110
            width: '1280'
          - filename: fda-antismoking-science-class-960x540.mp4
            format: mp4
            height: '540'
            size: 4252938
            width: '960'
          - filename: fda-antismoking-science-class-640x360.mp4
            format: mp4
            height: '360'
            size: 2878854
            width: '640'
          - filename: fda-antismoking-science-class-480x270.ogv
            format: ogv
            height: '270'
            size: 2721150
            width: '480'
          - filename: fda-antismoking-science-class-640x360.webm
            format: webm
            height: '360'
            size: 2665369
            width: '640'
          - filename: fda-antismoking-science-class-480x270.mp4
            format: mp4
            height: '270'
            size: 2024248
            width: '480'
      title: Fda Antismoking Science Class
credits: []
---
