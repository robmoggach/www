---
title: VW Passat
subtitle: Question
slug: vw-passat-question
date: 2015-06-20
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: vw-passat-question.01.jpg
          - caption: ''
            filename: vw-passat-question.02.jpg
          - caption: ''
            filename: vw-passat-question.03.jpg
          - caption: ''
            filename: vw-passat-question.04.jpg
          - caption: ''
            filename: vw-passat-question.05.jpg
          - caption: ''
            filename: vw-passat-question.06.jpg
          - caption: ''
            filename: vw-passat-question.07.jpg
      path: work/2015/vw-passat-question/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/vw-passat-question/videos/vw-passat-question.jpg
      path: work/2015/vw-passat-question/videos/vw-passat-question
      slug: vw-passat-question
      provider: html5
      sources:
          - filename: vw-passat-question-1280x720.mp4
            format: mp4
            height: '720'
            size: 14520578
            width: '1280'
          - filename: vw-passat-question-960x540.mp4
            format: mp4
            height: '540'
            size: 10913937
            width: '960'
          - filename: vw-passat-question-640x360.ogv
            format: ogv
            height: '360'
            size: 7317830
            width: '640'
          - filename: vw-passat-question-640x360.mp4
            format: mp4
            height: '360'
            size: 7220145
            width: '640'
          - filename: vw-passat-question-640x360.webm
            format: webm
            height: '360'
            size: 6352521
            width: '640'
          - filename: vw-passat-question-480x270.mp4
            format: mp4
            height: '270'
            size: 4837717
            width: '480'
      title: Vw Passat Question
credits:
    - companyName: JAMM
      companySlug: jamm
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - personName: Asher Edwards
      personSlug: asher-edwards
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: false
---
