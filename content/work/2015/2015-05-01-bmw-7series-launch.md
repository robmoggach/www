---
title: BMW 7 Series | '"Ma", "Xu" & "Yu"'
subtitle: '"Ma", "Xu" & "Yu"'
slug: bmw-7series-launch-ma-xu-yu
date: 2015-05-01
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - Colour
    - Compositing
    - Automotive
    - Set Extensions
    - VFX
albums:
    - cover:
      images:
          - caption: ''
            filename: bmw-7series-launch-ma.01.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.02.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.03.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.04.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.05.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.06.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.07.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.08.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.09.jpg
          - caption: ''
            filename: bmw-7series-launch-ma.10.jpg
      path: work/2015/bmw-7series-launch-ma/images/bmw-7series-launch-ma-stills
      slug: bmw-7series-launch-ma-stills
      title: Bmw 7Series Launch Ma Stills
    - cover:
      images:
          - caption: ''
            filename: bmw-7series-launch-xu.01.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.02.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.03.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.04.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.05.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.06.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.07.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.08.jpg
          - caption: ''
            filename: bmw-7series-launch-xu.09.jpg
      path: work/2015/bmw-7series-launch-ma/images/bmw-7series-launch-xu-stills
      slug: bmw-7series-launch-xu-stills
      title: Bmw 7Series Launch Xu Stills
    - cover:
      images:
          - caption: ''
            filename: bmw-7series-launch-yu.01.jpg
          - caption: ''
            filename: bmw-7series-launch-yu.02.jpg
          - caption: ''
            filename: bmw-7series-launch-yu.03.jpg
          - caption: ''
            filename: bmw-7series-launch-yu.04.jpg
          - caption: ''
            filename: bmw-7series-launch-yu.05.jpg
          - caption: ''
            filename: bmw-7series-launch-yu.06.jpg
          - caption: ''
            filename: bmw-7series-launch-yu.07.jpg
      path: work/2015/bmw-7series-launch-ma/images/bmw-7series-launch-yu-stills
      slug: bmw-7series-launch-yu-stills
      title: Bmw 7Series Launch Yu Stills
videos:
    - caption: ''
      cover: work/2015/bmw-7series-launch-ma/videos/bmw-7series-launch-ma.jpg
      path: work/2015/bmw-7series-launch-ma/videos/bmw-7series-launch-ma
      provider: html5
      slug: bmw-7series-launch-ma
      sources:
          - filename: bmw-7series-ma-1280x720.mp4
            format: mp4
            height: '720'
            size: 18656920
            width: '1280'
          - filename: bmw-7series-ma-960x540.mp4
            format: mp4
            height: '540'
            size: 14305042
            width: '960'
          - filename: bmw-7series-ma-640x360.ogv
            format: ogv
            height: '360'
            size: 12698854
            width: '640'
          - filename: bmw-7series-ma-640x360.mp4
            format: mp4
            height: '360'
            size: 9941993
            width: '640'
          - filename: bmw-7series-ma-480x270.mp4
            format: mp4
            height: '270'
            size: 7397905
            width: '480'
      title: Bmw 7Series Launch Ma
    - caption: ''
      cover: work/2015/bmw-7series-launch-ma/videos/bmw-7series-launch-xu.jpg
      path: work/2015/bmw-7series-launch-ma/videos/bmw-7series-launch-xu
      provider: html5
      slug: bmw-7series-launch-xu
      sources:
          - filename: bmw-7series-xu-1280x720.mp4
            format: mp4
            height: '720'
            size: 14685628
            width: '1280'
          - filename: bmw-7series-xu-960x540.mp4
            format: mp4
            height: '540'
            size: 11811893
            width: '960'
          - filename: bmw-7series-xu-640x360.ogv
            format: ogv
            height: '360'
            size: 9684846
            width: '640'
          - filename: bmw-7series-xu-640x360.webm
            format: webm
            height: '360'
            size: 8840650
            width: '640'
          - filename: bmw-7series-xu-640x360.mp4
            format: mp4
            height: '360'
            size: 8081764
            width: '640'
          - filename: bmw-7series-xu-480x270.mp4
            format: mp4
            height: '270'
            size: 6037961
            width: '480'
      title: Bmw 7Series Launch Xu
    - caption: ''
      cover: work/2015/bmw-7series-launch-ma/videos/bmw-7series-launch-yu.jpg
      path: work/2015/bmw-7series-launch-ma/videos/bmw-7series-launch-yu
      provider: html5
      slug: bmw-7series-launch-yu
      sources:
          - filename: bmw-7series-yu-1280x720.mp4
            format: mp4
            height: '720'
            size: 11983483
            width: '1280'
          - filename: bmw-7series-yu-960x540.mp4
            format: mp4
            height: '540'
            size: 8950769
            width: '960'
          - filename: bmw-7series-yu-640x360.ogv
            format: ogv
            height: '360'
            size: 6977530
            width: '640'
          - filename: bmw-7series-yu-640x360.webm
            format: webm
            height: '360'
            size: 6248012
            width: '640'
          - filename: bmw-7series-yu-640x360.mp4
            format: mp4
            height: '360'
            size: 5821127
            width: '640'
          - filename: bmw-7series-yu-480x270.mp4
            format: mp4
            height: '270'
            size: 4329418
            width: '480'
      title: Bmw 7Series Launch Yu
credits:
    - companyName: Gwantsi Shanghai
      companySlug: gwantsi-shanghai
      roleSlug: production
      roleTitle: Production Company
      visible: true
    - companyName: Gwantsi Shanghai
      companySlug: gwantsi-shanghai
      personName: Bo Platt
      personSlug: bo-platt
      roleSlug: director
      roleTitle: Director
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: colorist
      roleTitle: Colorist
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: false
---
