---
title: Cadillac CT6
subtitle: Grand Entrance
slug: cadillac-ct6-grand-entrance
date: 2015-07-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
    - Advertising
tags:
    - automotive
albums:
    - cover:
      images:
          - caption: ''
            filename: cadillac-ct6-grand-entrance.01.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.02.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.03.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.04.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.05.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.06.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.07.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.08.jpg
          - caption: ''
            filename: cadillac-ct6-grand-entrance.09.jpg
      path: work/2015/cadillac-ct6-grand-entrance/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/cadillac-ct6-grand-entrance/videos/cadillac-ct6-grand-entrance.jpg
      path: work/2015/cadillac-ct6-grand-entrance/videos/cadillac-ct6-grand-entrance
      slug: cadillac-ct6-grand-entrance
      provider: html5
      sources:
          - filename: cadillac-ct6-grand-entrance-1280x720.mp4
            format: mp4
            height: '720'
            size: 14243466
            width: '1280'
          - filename: cadillac-ct6-grand-entrance-960x540.mp4
            format: mp4
            height: '540'
            size: 11227167
            width: '960'
          - filename: cadillac-ct6-grand-entrance-640x360.webm
            format: webm
            height: '360'
            size: 8116165
            width: '640'
          - filename: cadillac-ct6-grand-entrance-640x360.mp4
            format: mp4
            height: '360'
            size: 8050278
            width: '640'
          - filename: cadillac-ct6-grand-entrance-480x270.ogv
            format: ogv
            height: '270'
            size: 7950749
            width: '480'
          - filename: cadillac-ct6-grand-entrance-480x270.mp4
            format: mp4
            height: '270'
            size: 6061505
            width: '480'
      title: Cadillac Ct6 Grand Entrance
credits: []
---
