---
title: FDA Anti-Smoking | "Found It"
subtitle: 'Found It'
slug: fda-antismoking-found-it
date: 2015-02-01
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: fda-antismoking-found-it.01.jpg
          - caption: ''
            filename: fda-antismoking-found-it.02.jpg
          - caption: ''
            filename: fda-antismoking-found-it.03.jpg
          - caption: ''
            filename: fda-antismoking-found-it.04.jpg
          - caption: ''
            filename: fda-antismoking-found-it.05.jpg
          - caption: ''
            filename: fda-antismoking-found-it.06.jpg
          - caption: ''
            filename: fda-antismoking-found-it.07.jpg
      path: work/2015/fda-antismoking-found-it/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/fda-antismoking-found-it/videos/fda-antismoking-found-it.jpg
      path: work/2015/fda-antismoking-found-it/videos/fda-antismoking-found-it
      slug: fda-antismoking-found-it
      provider: html5
      sources:
          - filename: fda-antismoking-found-it-1280x720.mp4
            format: mp4
            height: '720'
            size: 6409354
            width: '1280'
          - filename: fda-antismoking-found-it-960x540.mp4
            format: mp4
            height: '540'
            size: 4892880
            width: '960'
          - filename: fda-antismoking-found-it-640x360.mp4
            format: mp4
            height: '360'
            size: 3164862
            width: '640'
          - filename: fda-antismoking-found-it-480x270.ogv
            format: ogv
            height: '270'
            size: 2900237
            width: '480'
          - filename: fda-antismoking-found-it-640x360.webm
            format: webm
            height: '360'
            size: 2542246
            width: '640'
          - filename: fda-antismoking-found-it-480x270.mp4
            format: mp4
            height: '270'
            size: 2200460
            width: '480'
      title: Fda Antismoking Found It
credits:
    - companyName: MassMarket
      companySlug: massmarket
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: false
    - companyName: MassMarket
      companySlug: massmarket
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: creative-director
      roleTitle: Creative Director
      visible: false
    - companyName: MassMarket
      companySlug: massmarket
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: false
---
