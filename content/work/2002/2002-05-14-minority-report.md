---
title: Minority Report
subtitle:
slug: minority-report
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: minority-report.01.jpg
    -   caption: ''
        filename: minority-report.02.jpg
    -   caption: ''
        filename: minority-report.03.jpg
    path: work/2002/minority-report/images/stills
    slug: stills
    title: Stills
videos: []
credits: []
---
