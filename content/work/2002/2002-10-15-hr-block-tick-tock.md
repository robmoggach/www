---
title: Hr Block Tick Tock
subtitle:
slug: hr-block-tick-tock
date: 2002-10-01
role:
headline: directed by Jim Sonzero, RSA Films
summary: The world anticipates a man's tax return like a ticking clock.
excerpt: The world anticipates a man's tax return like a ticking clock.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: hr-block-tick-tock.01.jpg
    -   caption: ''
        filename: hr-block-tick-tock.02.jpg
    -   caption: ''
        filename: hr-block-tick-tock.03.jpg
    -   caption: ''
        filename: hr-block-tick-tock.04.jpg
    -   caption: ''
        filename: hr-block-tick-tock.05.jpg
    path: work/2002/hr-block-tick-tock/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/hr-block-tick-tock/videos/hr-block-tick-tock.jpg
    path: work/2002/hr-block-tick-tock/videos/hr-block-tick-tock
    slug: hr-block-tick-tock
    sources:
    -   filename: hr-block-tick-tock-1280x720.mp4
        format: mp4
        height: '720'
        size: 9754238
        width: '1280'
    -   filename: hr-block-tick-tock-960x540.mp4
        format: mp4
        height: '540'
        size: 6270161
        width: '960'
    -   filename: hr-block-tick-tock-640x360.ogv
        format: ogv
        height: '360'
        size: 4327843
        width: '640'
    -   filename: hr-block-tick-tock-640x360.mp4
        format: mp4
        height: '360'
        size: 3691611
        width: '640'
    -   filename: hr-block-tick-tock-640x360.webm
        format: webm
        height: '360'
        size: 3158778
        width: '640'
    -   filename: hr-block-tick-tock-480x270.mp4
        format: mp4
        height: '270'
        size: 2389999
        width: '480'
    title: Hr Block Tick Tock
credits:
-   companyName: H & R Block
    companySlug: h-r-block
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Jim Sonzero
    personSlug: jim-sonzero
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
## The world anticipates a man's tax return like a ticking clock.