---
title: Hertz Swimmer
subtitle:
slug: hertz-swimmer
date: 2002-08-01
role:
headline: directed by Michael Karbelnikoff, HKM
summary: Man swims lost in an underwater parking lot looking for his car.
excerpt: Man swims lost in an underwater parking lot looking for his car.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- environment
- under water
- set extensions
- photogrammetry
albums:
-   cover:
    images:
    -   caption: ''
        filename: hertz-swimmer.01.jpg
    -   caption: ''
        filename: hertz-swimmer.02.jpg
    -   caption: ''
        filename: hertz-swimmer.03.jpg
    -   caption: ''
        filename: hertz-swimmer.04.jpg
    path: work/2002/hertz-swimmer/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/hertz-swimmer/videos/hertz-swimmer.jpg
    path: work/2002/hertz-swimmer/videos/hertz-swimmer
    slug: hertz-swimmer
    sources:
    -   filename: hertz-swimmer.1280x960.mp4
        format: mp4
        height: '960'
        size: 10982441
        width: '1280'
    -   filename: hertz-swimmer.960x720.mp4
        format: mp4
        height: '720'
        size: 10663882
        width: '960'
    -   filename: hertz-swimmer.640x480.mp4
        format: mp4
        height: '480'
        size: 5493416
        width: '640'
    -   filename: hertz-swimmer.640x360.ogv
        format: ogv
        height: '360'
        size: 5272546
        width: '640'
    -   filename: hertz-swimmer.640x360.webm
        format: webm
        height: '360'
        size: 5245897
        width: '640'
    -   filename: hertz-swimmer.480x360.mp4
        format: mp4
        height: '360'
        size: 4548411
        width: '480'
    title: Hertz Swimmer
credits:
-   companyName: Hertz
    companySlug: hertz
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: HKM Productions
    companySlug: hkm-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Michael Karbelnikoff
    personSlug: michael-karbelnikoff
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
For this commercial we had the monumental task of creating an underwater
environment where before was only a non-descript rental car parking lot. A man
swims through this murky underwater environment searching for his car without
luck. This was a great challenge. The only underwater element I was supplied
with was the man swimming in a tank. The cars, parking lot, bubbles and
caustic lighting were all created in flame using a combination of elements
generated in flame and minimal CGI. Every shot involved isolating each vehicle
independently and applying different localized colour correction to create a
more underwater feel. Caustic lighting elements were generated in flame and
mapped on to CG cars to generate the characteristic lighting. This was then
applied to the previously corrected cars to create the look. After this step
came the task of creating streams of photo-realistic bubbles in flame tracked
perfectly to the scene. Shafts of light and small bits of floating debris were
then added in the final stage during which the live action man was composited
into the scene. Other elements like the surface of the water, the fog that
fades into background and reflections were then added as a final touch to
increase the realism.