---
title: Hyundai | "Urban Test Facility"
subtitle: Urban Test Facility
slug: hyundai-urban-test-facility
date: 2012-07-16
role: 
headline: ''
summary: "Elantra GT twists and turns through projections of the outside\rworld until
  magically we are driving down a residential street."
excerpt: "Elantra GT twists and turns through projections of the outside\rworld until
  magically we are driving down a residential street."
published: true
featured: true
categories:
- Advertising
tags:
- animation
- automotive
- CG
- compositing
- design
- effects
- environment
- VFX
- photoreal
- projection
- colour
albums:
- cover: 
  images:
  - caption: ''
    filename: hyundai-urban-test-facility.01.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.02.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.03.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.04.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.05.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.06.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.07.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.08.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.09.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.10.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.11.jpg
  - caption: ''
    filename: hyundai-urban-test-facility.12.jpg
  path: work/2012/hyundai-urban-test-facility/images/stills
  slug: stills
  title: Stills
videos:
- caption: ''
  cover: work/2012/hyundai-urban-test-facility/videos/hyundai-urban-test-facility.jpg
  path: work/2012/hyundai-urban-test-facility/videos/hyundai-urban-test-facility
  slug: hyundai-urban-test-facility
  sources:
  - filename: hyundai-urban-test-facility-1280x720.mp4
    format: mp4
    height: '720'
    size: 20131611
    width: '1280'
  - filename: hyundai-urban-test-facility-960x540.mp4
    format: mp4
    height: '540'
    size: 5375299
    width: '960'
  - filename: hyundai-urban-test-facility-640x360.ogv
    format: ogv
    height: '360'
    size: 3954241
    width: '640'
  - filename: hyundai-urban-test-facility-640x360.mp4
    format: mp4
    height: '360'
    size: 3653776
    width: '640'
  - filename: hyundai-urban-test-facility-640x360.webm
    format: webm
    height: '360'
    size: 3443976
    width: '640'
  - filename: hyundai-urban-test-facility-480x270.mp4
    format: mp4
    height: '270'
    size: 2540187
    width: '480'
  title: Hyundai Urban Test Facility
credits:
- companyName: Hyundai Canada
  companySlug: hyundai-canada
  roleSlug: client
  roleTitle: Client
  visible: false
- companyName: Innocean Worldwide Canada
  companySlug: innocean-worldwide-canada
  roleSlug: agency
  roleTitle: Advertising Agency
  visible: false
- personName: Gary Westgate
  personSlug: gary-westgate
  roleSlug: agency-cd
  roleTitle: Creative Director
  visible: false
- personName: Damon Crate
  personSlug: damon-crate
  roleSlug: agency-art-director
  roleTitle: Art Director
  visible: false
- personName: Nelson Quintal
  personSlug: nelson-quintal
  roleSlug: agency-copywriter
  roleTitle: Copywriter
  visible: false
- personName: Alina Prussky
  personSlug: alina-prussky
  roleSlug: agency-producer
  roleTitle: Agency Producer
  visible: false
- companyName: Soft Citizen
  companySlug: soft-citizen
  roleSlug: production
  roleTitle: Production Company
  visible: false
- personName: Chris Sargent
  personSlug: chris-sargent
  roleSlug: director
  roleTitle: Director
  visible: false
- personName: Stuart Graham
  personSlug: stuart-graham
  roleSlug: dp
  roleTitle: Director of Photography
  visible: false
- personName: Link York
  personSlug: link-york
  roleSlug: prod-ep
  roleTitle: Executive Producer
  visible: false
- personName: Tony DiMarco
  personSlug: tony-dimarco
  roleSlug: line-producer
  roleTitle: Line Producer
  visible: false
- companyName: coverboy
  companySlug: coverboy
  roleSlug: edit
  roleTitle: Editorial
  visible: false
- personName: Mark Paiva
  personSlug: mark-paiva
  roleSlug: editor
  roleTitle: Editor
  visible: false
- personName: Michelle Lee
  personSlug: michelle-lee
  roleSlug: edit-ep
  roleTitle: Executive Producer
  visible: false
- companyName: Dashing Collective
  companySlug: dshng
  roleSlug: vfx
  roleTitle: Visual Effects
  visible: false
- personName: Robert Moggach
  personSlug: robert-moggach
  roleSlug: vfx-supervisor
  roleTitle: VFX Supervisor
  visible: false
- personName: Mary Anne Ledesma
  personSlug: mary-anne-ledesma
  roleSlug: vfx-producer
  roleTitle: VFX Producer
  visible: false
- personName: Sebastian Bilbao
  personSlug: sebastian-bilbao
  roleSlug: cg-sup
  roleTitle: CG Supervisor
  visible: false
- personName: Aylwin Fernando
  personSlug: aylwin-fernando
  roleSlug: cg-lighting
  roleTitle: CG Lighting
  visible: false

---
As part of a continuing creative collaboration with Innocean Canada, Dashing
re-visited Hyundai's test facility to put the new Elantra GT through it's
paces. Brought to life by Chris Sargent, we see the GT twist and turn through
filmic projections of the outside world until magically we are driving down a
residential street.

With a month long schedule, it was our huge task to ensure the quality of the photo-real environments and visual aesthetic matched the demanding needs of creative.

It was great to be able to revisit what we created in the Engineering
campaign and evolve the environment, adding details and texture that made it
feel even more grounded in reality. The filmic grit of director Chris
Sargent's style made it an interesting creative departure, introducing light,
flaring and grain that contrasted the very clean, futuristic visuals from the
original campaign.