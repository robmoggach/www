---
title: Cineplex Escape
subtitle:
slug: cineplex-escape
date: 2012-08-02
role:
headline: ''
summary: Spy Films and Dashing create the ultimate movie-going experience.
excerpt: Spy Films and Dashing create the ultimate movie-going experience.
published: false
featured: false
categories:
- Advertising
tags:
- CG
- VFX
- animation
- CG
- effects
- design
- compositing
- environment
- clouds
- earth
- nebula
- photoreal
- colour
albums:
-   cover:
    images:
    -   caption: ''
        filename: cineplex-escape.01.jpg
    -   caption: ''
        filename: cineplex-escape.02.jpg
    -   caption: ''
        filename: cineplex-escape.03.jpg
    -   caption: ''
        filename: cineplex-escape.04.jpg
    -   caption: ''
        filename: cineplex-escape.05.jpg
    -   caption: ''
        filename: cineplex-escape.06.jpg
    -   caption: ''
        filename: cineplex-escape.07.jpg
    -   caption: ''
        filename: cineplex-escape.08.jpg
    -   caption: ''
        filename: cineplex-escape.09.jpg
    -   caption: ''
        filename: cineplex-escape.10.jpg
    -   caption: ''
        filename: cineplex-escape.11.jpg
    -   caption: ''
        filename: cineplex-escape.12.jpg
    -   caption: ''
        filename: cineplex-escape.13.jpg
    -   caption: ''
        filename: cineplex-escape.14.jpg
    path: work/2012/cineplex-escape/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2012/cineplex-escape/videos/cineplex-esc020-making-of.jpg
    path: work/2012/cineplex-escape/videos/cineplex-esc020-making-of
    slug: cineplex-esc020-making-of
    sources:
    -   filename: cineplex-esc020-making-of-960x540.mp4
        format: mp4
        height: '540'
        size: 3730355
        width: '960'
    -   filename: cineplex-esc020-making-of-640x360.mp4
        format: mp4
        height: '360'
        size: 2507008
        width: '640'
    -   filename: cineplex-esc020-making-of-640x360.ogv
        format: ogv
        height: '360'
        size: 1535131
        width: '640'
    -   filename: cineplex-esc020-making-of-480x270.mp4
        format: mp4
        height: '270'
        size: 1405268
        width: '480'
    -   filename: cineplex-esc020-making-of-640x360.webm
        format: webm
        height: '360'
        size: 1396616
        width: '640'
    title: Cineplex Esc020 Making Of
-   caption: ''
    cover: work/2012/cineplex-escape/videos/cineplex-esc070-making-of.jpg
    path: work/2012/cineplex-escape/videos/cineplex-esc070-making-of
    slug: cineplex-esc070-making-of
    sources:
    -   filename: cineplex-esc070-making-of-960x540.mp4
        format: mp4
        height: '540'
        size: 4344729
        width: '960'
    -   filename: cineplex-esc070-making-of-640x360.mp4
        format: mp4
        height: '360'
        size: 3275492
        width: '640'
    -   filename: cineplex-esc070-making-of-640x360.ogv
        format: ogv
        height: '360'
        size: 2241409
        width: '640'
    -   filename: cineplex-esc070-making-of-480x270.mp4
        format: mp4
        height: '270'
        size: 1944723
        width: '480'
    -   filename: cineplex-esc070-making-of-640x360.webm
        format: webm
        height: '360'
        size: 1774235
        width: '640'
    title: Cineplex Esc070 Making Of
-   caption: ''
    cover: work/2012/cineplex-escape/videos/cineplex-esc070-making-of-cloudEnv.jpg
    path: work/2012/cineplex-escape/videos/cineplex-esc070-making-of-cloudEnv
    slug: cineplex-esc070-making-of-cloudEnv
    sources:
    -   filename: cineplex-esc070-making-of-cloudEnv-960x540.mp4
        format: mp4
        height: '540'
        size: 403060
        width: '960'
    -   filename: cineplex-esc070-making-of-cloudEnv-640x360.mp4
        format: mp4
        height: '360'
        size: 266051
        width: '640'
    -   filename: cineplex-esc070-making-of-cloudEnv-640x360.ogv
        format: ogv
        height: '360'
        size: 230408
        width: '640'
    -   filename: cineplex-esc070-making-of-cloudEnv-640x360.webm
        format: webm
        height: '360'
        size: 220133
        width: '640'
    -   filename: cineplex-esc070-making-of-cloudEnv-480x270.mp4
        format: mp4
        height: '270'
        size: 173940
        width: '480'
    title: Cineplex Esc070 Making Of Cloudenv
-   caption: ''
    cover: work/2012/cineplex-escape/videos/cineplex-esc120-making-of.jpg
    path: work/2012/cineplex-escape/videos/cineplex-esc120-making-of
    slug: cineplex-esc120-making-of
    sources:
    -   filename: cineplex-esc120-making-of-960x540.mp4
        format: mp4
        height: '540'
        size: 2685260
        width: '960'
    -   filename: cineplex-esc120-making-of-640x360.mp4
        format: mp4
        height: '360'
        size: 1945060
        width: '640'
    -   filename: cineplex-esc120-making-of-640x360.ogv
        format: ogv
        height: '360'
        size: 1669184
        width: '640'
    -   filename: cineplex-esc120-making-of-640x360.webm
        format: webm
        height: '360'
        size: 1255862
        width: '640'
    -   filename: cineplex-esc120-making-of-480x270.mp4
        format: mp4
        height: '270'
        size: 1232995
        width: '480'
    title: Cineplex Esc120 Making Of
-   caption: ''
    cover: work/2012/cineplex-escape/videos/cineplex-escape.jpg
    path: work/2012/cineplex-escape/videos/cineplex-escape
    slug: cineplex-escape
    sources:
    -   filename: cineplex-escape-1280x720.mp4
        format: mp4
        height: '720'
        size: 26075151
        width: '1280'
    -   filename: cineplex-escape-960x540.mp4
        format: mp4
        height: '540'
        size: 9054486
        width: '960'
    -   filename: cineplex-escape-640x360.mp4
        format: mp4
        height: '360'
        size: 5612852
        width: '640'
    -   filename: cineplex-escape-640x360.ogv
        format: ogv
        height: '360'
        size: 5078551
        width: '640'
    -   filename: cineplex-escape-640x360.webm
        format: webm
        height: '360'
        size: 4246783
        width: '640'
    -   filename: cineplex-escape-480x270.mp4
        format: mp4
        height: '270'
        size: 3634707
        width: '480'
    title: Cineplex Escape
credits:
-   companyName: Cineplex
    companySlug: cineplex
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Spy Films
    companySlug: spy-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Arev Manoukian
    personSlug: arev-manoukian
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Carlo Trulli
    personSlug: carlo-trulli
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Marcus Trulli
    personSlug: marcus-trulli
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: creative-director
    roleTitle: Creative Director
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
-   personName: Sebastian Bilbao
    personSlug: sebastian-bilbao
    roleSlug: cg-sup
    roleTitle: CG Supervisor
    visible: false
-   personName: Aylwin Fernando
    personSlug: aylwin-fernando
    roleSlug: cg-lighting
    roleTitle: CG Lighting
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: animator
    roleTitle: CG Animator
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: fx-td
    roleTitle: FX Technical Director
    visible: false
-   companyName: BoomBox Sound
    companySlug: boombox-sound
    roleSlug: sound-design
    roleTitle: Sound Design
    visible: false
-   personName: Roger Leavens
    personSlug: roger-leavens
    roleSlug: sound-dsgnr
    roleTitle: Sound Designer
    visible: false
-   personName: Umber Hamid
    personSlug: umber-hamid
    roleSlug: sound-ep
    roleTitle: Executive Producer
    visible: false
---
Spy Films and Dashing came together to create the ultimate movie-going
experience. Through months of arduous labour, the diverse team has completed a
journey into the celebration of Cineplex's 100 years of film. The theatrical
brand spot is an immersive and emotional audio-visual experience that
solidifies Cineplex as the leader for a new generation of moviegoers.

The mini-film begins in black and white with the Wright Flyer taking off at
Kitty Hawk, NC. Over the course of 40 seconds we see the film evolve into full
frame, technicolour, mono to 7.1 sound and finally, stereoscopic 3D. All the
while, the planes of history are also evolvingâ€¦from the Wright Bros to the
NASA space shuttle.

We were excited for this project
because from the very beginning of the creative process it was truly
collaborative. We literally used every creative tool at our disposal and then
created some of our own. The clouds and nebulae were created using custom
software developed for this project. I worked alongside my large team animating, lighting and compositing.

The mini-film played throughout all Cineplex locations nation-wide
more than 3 years. 15 million viewers a month saw the finished film.