---
title: Desert Girl Vr
subtitle:
slug: desert-girl-vr
date: 2016-06-20
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
    - Virtual Reality
tags: []
albums:
    - cover:
      images: []
      path: work/2016/desert-girl-vr/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: ''
      path: work/2016/desert-girl-vr/videos/desert-girl-vr
      slug: desert-girl-vr
      sources: []
      title: Desert Girl Vr
credits: []
---

This was a simple yet elegant combination of multiple elements shot in camera.
The various elements were first cleaned of any imperfections and then combined
using a variety of mixes and layering.
