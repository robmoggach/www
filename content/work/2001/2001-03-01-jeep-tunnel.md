---
title: Jeep Tunnel
subtitle:
slug: jeep-tunnel
date: 2001-03-01
role:
headline: directed by Gerard De Thame, GDT Films
summary: New Jeep Liberty takes on all challenges.
excerpt: New Jeep Liberty takes on all challenges.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: jeep-tunnel.01.jpg
    -   caption: ''
        filename: jeep-tunnel.02.jpg
    -   caption: ''
        filename: jeep-tunnel.03.jpg
    -   caption: ''
        filename: jeep-tunnel.04.jpg
    -   caption: ''
        filename: jeep-tunnel.05.jpg
    -   caption: ''
        filename: jeep-tunnel.06.jpg
    -   caption: ''
        filename: jeep-tunnel.07.jpg
    path: work/2001/jeep-tunnel/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/jeep-tunnel/videos/jeep-tunnel.jpg
    path: work/2001/jeep-tunnel/videos/jeep-tunnel
    slug: jeep-tunnel
    sources:
    -   filename: jeep-tunnel.960x720.mp4
        format: mp4
        height: '720'
        size: 10997711
        width: '960'
    -   filename: jeep-tunnel.1280x960.mp4
        format: mp4
        height: '960'
        size: 9841963
        width: '1280'
    -   filename: jeep-tunnel.640x360.ogv
        format: ogv
        height: '360'
        size: 4916087
        width: '640'
    -   filename: jeep-tunnel.640x360.webm
        format: webm
        height: '360'
        size: 4895027
        width: '640'
    -   filename: jeep-tunnel.480x360.mp4
        format: mp4
        height: '360'
        size: 4823693
        width: '480'
    -   filename: jeep-tunnel.640x480.mp4
        format: mp4
        height: '480'
        size: 4792116
        width: '640'
    title: Jeep Tunnel
credits: []
---
This commercial depicts the new Jeep Liberty being challenged by nature. As it
approaches a tunnel in the side of a mountain, the mountain shifts into the
sea, forcing the Jeep to drive over the mountain, as opposed to through the
tunnel. Every shot in this commercial involved effects of some kind. Multiple
passes of a red Jeep were telecine'd for beauty for the different body parts.
Two versions of the commercial were created-one with a black Jeep, one with a
red Jeep but only a red Jeep was shot. In addition to creating two differently
coloured versions we had the task of creating an ocean-side road where no
ocean existed and creating a tunnel that shifted to force the Jeep to drive
over the mountain. Shot on a concrete incline, we also had to create a rough
rock surface beneath the Jeep. 3D tracking data and hand tracked cameras were
used in inferno to introduce hand painted and displaced rock surfaces to the
scene. Additional dust and rock elements were added to cement the shots.