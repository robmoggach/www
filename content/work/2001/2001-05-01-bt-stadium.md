---
title: BT "Stadium"
subtitle:
slug: bt-stadium
date: 2001-05-01
role:
headline: directed by Jake Scott, RSA Films
summary: British Telecom enables a global audience in a giant stadium.
excerpt: British Telecom enables a global audience in a giant stadium.
published: false
featured: false
categories: []
tags:
- crowds
- matte painting
- clouds
- compositing
- photogrammetry
albums:
-   cover:
    images:
    -   caption: ''
        filename: bt-stadium.01.jpg
    -   caption: ''
        filename: bt-stadium.02.jpg
    -   caption: ''
        filename: bt-stadium.03.jpg
    -   caption: ''
        filename: bt-stadium.04.jpg
    -   caption: ''
        filename: bt-stadium.05.jpg
    -   caption: ''
        filename: bt-stadium.06.jpg
    path: work/2001/bt-stadium/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/bt-stadium/videos/bt-stadium.jpg
    path: work/2001/bt-stadium/videos/bt-stadium
    slug: bt-stadium
    sources:
    -   filename: bt-stadium-1280x720.mp4
        format: mp4
        height: '720'
        size: 31723718
        width: '1280'
    -   filename: bt-stadium-960x540.mp4
        format: mp4
        height: '540'
        size: 21761686
        width: '960'
    -   filename: bt-stadium-640x360.ogv
        format: ogv
        height: '360'
        size: 14003585
        width: '640'
    -   filename: bt-stadium-640x360.mp4
        format: mp4
        height: '360'
        size: 13740583
        width: '640'
    -   filename: bt-stadium-640x360.webm
        format: webm
        height: '360'
        size: 10571537
        width: '640'
    -   filename: bt-stadium-480x270.mp4
        format: mp4
        height: '270'
        size: 8910883
        width: '480'
    title: Bt Stadium
credits: []
---
To tell the story of British Telecom allowing it's clients to reach the global
marketplace, director Jake Scott from RSA/Black Dog developed the concept of a
massive stadium where those with a product or even a question could approach
the global audience. Creating this massive stadium was our task seeing as it
is completely fictional. Practically every shot in this commercial involved
multiple crowd replication shots for this infinitely vast fictional stadium.
The shots featured here are wide shots of the stadium that were created using,
at times, more than 40 layers of live action clouds, combined in inferno using
paint and selective matting. Sparks were used to enhance the look and give the
impression of the sun being off camera. The end result was a truly touching
story that showcases different individuals empowered by being able to reach
the audience they couldn't before.