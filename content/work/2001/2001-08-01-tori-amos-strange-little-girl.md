---
title: Tori Amos Strange Little Girl
subtitle:
slug: tori-amos-strange-little-girl
date: 2001-08-01
role:
headline: directed by David Slade, Bullett
summary: Tori Amos is haunted by wolves and childhood visions.
excerpt: Tori Amos is haunted by wolves and childhood visions.
published: false
featured: false
categories:
- Music Videos
tags:
- compositing
- matte painting
- miniatures
albums:
-   cover:
    images:
    -   caption: ''
        filename: tori-amos-strange-little-girl.01.jpg
    -   caption: ''
        filename: tori-amos-strange-little-girl.02.jpg
    -   caption: ''
        filename: tori-amos-strange-little-girl.03.jpg
    -   caption: ''
        filename: tori-amos-strange-little-girl.04.jpg
    -   caption: ''
        filename: tori-amos-strange-little-girl.05.jpg
    -   caption: ''
        filename: tori-amos-strange-little-girl.06.jpg
    -   caption: ''
        filename: tori-amos-strange-little-girl.07.jpg
    path: work/2001/tori-amos-strange-little-girl/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/tori-amos-strange-little-girl/videos/tori-amos-strange-little-girl.jpg
    path: work/2001/tori-amos-strange-little-girl/videos/tori-amos-strange-little-girl
    slug: tori-amos-strange-little-girl
    sources:
    -   filename: tori-amos-strange-little-girl.1280x960.mp4
        format: mp4
        height: '960'
        size: 68696044
        width: '1280'
    -   filename: tori-amos-strange-little-girl.960x720.mp4
        format: mp4
        height: '720'
        size: 43171991
        width: '960'
    -   filename: tori-amos-strange-little-girl.640x480.mp4
        format: mp4
        height: '480'
        size: 31319869
        width: '640'
    -   filename: tori-amos-strange-little-girl.640x360.ogv
        format: ogv
        height: '360'
        size: 27165250
        width: '640'
    -   filename: tori-amos-strange-little-girl.480x360.mp4
        format: mp4
        height: '360'
        size: 19282693
        width: '480'
    -   filename: tori-amos-strange-little-girl.640x360.webm
        format: webm
        height: '360'
        size: 18096139
        width: '640'
    title: Tori Amos Strange Little Girl
credits:
-   companyName: Bullett
    companySlug: bullett
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: David Slade
    personSlug: david-slade
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
---
For Tori Amos' new single "Strange Little Girls" director David Slade
envisioned a fictional landscape of blue wheat fields, bright red skies, and a
giant black wolf chasing Tori to her house in the country for the music video.
This required extensive visual effects throughout. One of the big tasks was
creating the surreal skies that were key to the look the director wanted to
achieve. These were painted on the massive backdrop on set and using inferno I
extended the skies past ceiling. The painted backdrop had no real life to it
so I also had to imbue motion into using live action cloud footage and by
warping the existing painted backdrop. The wolf in the video had to appear
massive. This was done for some shots using a miniature set within which the
wolf appeared to be twenty feet tall. In other shots it was isolated and
reintroduced into shots of a smaller scale, with appropriate non-existent
shadows, in Inferno. It also had to appear to be mouse-sized as it enters the
house. In a similar fashion, camera angles were matched for shots of different
scale and integrated. Other effects work included beauty / cosmetic work on
Tori throughout and computer generated wheat fields.