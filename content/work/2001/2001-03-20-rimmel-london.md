---
title: Rimmel London
subtitle:
slug: rimmel-london
date: 2001-03-20
role:
headline: directed by Don Cameron
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: rimmel-london.01.jpg
    -   caption: ''
        filename: rimmel-london.02.jpg
    -   caption: ''
        filename: rimmel-london.03.jpg
    path: work/2001/rimmel-london/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/rimmel-london/videos/rimmel-london.jpg
    path: work/2001/rimmel-london/videos/rimmel-london
    slug: rimmel-london
    sources:
    -   filename: rimmel-london.1280x720.mp4
        format: mp4
        height: '720'
        size: 2549831
        width: '1280'
    -   filename: rimmel-london.960x540.mp4
        format: mp4
        height: '540'
        size: 1561834
        width: '960'
    -   filename: rimmel-london.640x360.mp4
        format: mp4
        height: '360'
        size: 1024300
        width: '640'
    -   filename: rimmel-london.640x360.ogv
        format: ogv
        height: '360'
        size: 783625
        width: '640'
    -   filename: rimmel-london.480x270.mp4
        format: mp4
        height: '270'
        size: 610850
        width: '480'
    -   filename: rimmel-london.640x360.webm
        format: webm
        height: '360'
        size: 567860
        width: '640'
    title: Rimmel London
credits: []
---
