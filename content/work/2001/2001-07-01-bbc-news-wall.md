---
title: BBC News "Wall"
subtitle:
slug: bbc-news-wall
date: 2001-07-01
role:
headline: directed by Walter Stern, Academy Films
summary: BBC captures the fall of the Berlin Wall.
excerpt: BBC captures the fall of the Berlin Wall.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- tracking
- crowds
- matte painting
albums:
-   cover:
    images:
    -   caption: ''
        filename: bbc-news-wall.01.jpg
    -   caption: ''
        filename: bbc-news-wall.02.jpg
    -   caption: ''
        filename: bbc-news-wall.03.jpg
    -   caption: ''
        filename: bbc-news-wall.04.jpg
    path: work/2001/bbc-news-wall/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/bbc-news-wall/videos/bbc-news-wall.jpg
    path: work/2001/bbc-news-wall/videos/bbc-news-wall
    slug: bbc-news-wall
    sources:
    -   filename: bbc-news-wall.1280x720.mp4
        format: mp4
        height: '720'
        size: 21241295
        width: '1280'
    -   filename: bbc-news-wall.960x540.mp4
        format: mp4
        height: '540'
        size: 15678237
        width: '960'
    -   filename: bbc-news-wall.640x360.mp4
        format: mp4
        height: '360'
        size: 9127053
        width: '640'
    -   filename: bbc-news-wall.640x360.ogv
        format: ogv
        height: '360'
        size: 8153202
        width: '640'
    -   filename: bbc-news-wall.640x360.webm
        format: webm
        height: '360'
        size: 6330898
        width: '640'
    -   filename: bbc-news-wall.480x270.mp4
        format: mp4
        height: '270'
        size: 5560720
        width: '480'
    title: Bbc News Wall
credits:
-   companyName: BBC News
    companySlug: bbc-news
    roleSlug: client
    roleTitle: Client
    visible: true
-   companyName: Academy Films
    companySlug: academy-films
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Walter Stern
    personSlug: walter-stern
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: true
---
To depict the BBC's all-encompassing coverage of world events, the collapse of
the Berlin Wall was chosen as the subject of this commercial. A young man
leaves his house and travels through the streets of East Berlin to join a
crowd busy destroying the wall. Because the commercial was shot in 2001, East
Berlin has lost a lot of what made it look like it did. Practically every shot
of the commercial required visual effects work for this reason. An existing
section of the wall was used for the commercial but this did not extend off
camera and the guard towers no longer were in place. I extended the wall and
created new guard towers using a combination of shot elements and elements I
painted in Inferno. Graffiti and commercial advertising did not exist in East
Berlin before the collapse. Now it's everywhere and as a result almost every
shot in the commercial required that I remove graffiti and billboards.
Walter's documentary style requires a lot of hand held camera work which made
this task more difficult. Other effects work included creating a massive
crowd, adding graffiti to the wall on the West side, and multiple composition
changes involving moving and adding of buildings.