---
title: Gloss I Feel Love
subtitle:
slug: gloss-i-feel-love
date: 2001-06-01
role:
headline: directed by Don Cameron
summary:
excerpt:
published: false
featured: false
categories:
- Music Videos
tags:
- projection
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: gloss-i-feel-love.01.jpg
    -   caption: ''
        filename: gloss-i-feel-love.02.jpg
    -   caption: ''
        filename: gloss-i-feel-love.03.jpg
    -   caption: ''
        filename: gloss-i-feel-love.04.jpg
    -   caption: ''
        filename: gloss-i-feel-love.05.jpg
    -   caption: ''
        filename: gloss-i-feel-love.06.jpg
    -   caption: ''
        filename: gloss-i-feel-love.07.jpg
    path: work/2001/gloss-i-feel-love/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: ''
    path: work/2001/gloss-i-feel-love/videos/gloss-i-feel-love
    slug: gloss-i-feel-love
    sources: []
    title: Gloss I Feel Love
credits:
-   companyName: Method Films
    companySlug: method
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Donald Cameron
    personSlug: donald-cameron
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
---
