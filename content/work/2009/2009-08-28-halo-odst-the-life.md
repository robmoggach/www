---
title: Halo Odst The Life
subtitle:
slug: halo-odst-the-life
date: 2009-08-28
role:
headline: directed by Rupert Sanders, MJZ
summary: Rupert Sanders directs another epic Halo campaign.
excerpt: Rupert Sanders directs another epic Halo campaign.
published: false
featured: false
categories:
- Advertising
tags:
- video game
- VFX
- compositing
- photoreal
- environment
- set extensions
- CG
- effects
- award
- colour
albums:
-   cover:
    images:
    -   caption: ''
        filename: halo-odst-the-life.01.jpg
    -   caption: ''
        filename: halo-odst-the-life.02.jpg
    -   caption: ''
        filename: halo-odst-the-life.03.jpg
    -   caption: ''
        filename: halo-odst-the-life.04.jpg
    -   caption: ''
        filename: halo-odst-the-life.05.jpg
    -   caption: ''
        filename: halo-odst-the-life.06.jpg
    -   caption: ''
        filename: halo-odst-the-life.07.jpg
    -   caption: ''
        filename: halo-odst-the-life.08.jpg
    -   caption: ''
        filename: halo-odst-the-life.09.jpg
    -   caption: ''
        filename: halo-odst-the-life.10.jpg
    -   caption: ''
        filename: halo-odst-the-life.11.jpg
    -   caption: ''
        filename: halo-odst-the-life.12.jpg
    -   caption: ''
        filename: halo-odst-the-life.13.jpg
    -   caption: ''
        filename: halo-odst-the-life.14.jpg
    -   caption: ''
        filename: halo-odst-the-life.15.jpg
    -   caption: ''
        filename: halo-odst-the-life.16.jpg
    path: work/2009/halo-odst-the-life/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/halo-odst-the-life/videos/halo-odst-the-life.jpg
    path: work/2009/halo-odst-the-life/videos/halo-odst-the-life
    slug: halo-odst-the-life
    sources:
    -   filename: halo-odst-the-life-1280x720.mp4
        format: mp4
        height: '720'
        size: 51071675
        width: '1280'
    -   filename: halo-odst-the-life-960x540.mp4
        format: mp4
        height: '540'
        size: 32630805
        width: '960'
    -   filename: halo-odst-the-life-640x360.mp4
        format: mp4
        height: '360'
        size: 19447076
        width: '640'
    -   filename: halo-odst-the-life-640x360.ogv
        format: ogv
        height: '360'
        size: 14968763
        width: '640'
    -   filename: halo-odst-the-life-640x360.webm
        format: webm
        height: '360'
        size: 12093074
        width: '640'
    -   filename: halo-odst-the-life-480x270.mp4
        format: mp4
        height: '270'
        size: 11740809
        width: '480'
    title: Halo Odst The Life
credits:
-   companyName: HALO ODST
    companySlug: halo-odst
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Rupert Sanders
    personSlug: rupert-sanders
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
We follow the life of an ODST (Orbital Drop Ship Trooper) in the Halo 3
universe from trainee to hardened veteran. This epic 2-1/2 minute spot for
Halo 3: ODST was as intense a film to produce as it is in it's finished form.
Shot over 4 days in the depths of a working open-pit coal mine, a working
power plant cooling tower, and an abandoned communist era metallurgy factory
the crew endured blistering 110 degree Fahrenheit heat that overnight turned
to 45 degree torrential rain storms with hurricane winds. The grit of these
extreme conditions shone through to create a film that is true to the visceral
aesthetic essential to it's success. With an aggressive schedule for
completion but under slightly more comfortable air-conditioned, Aeron-chaired,
and caffeinated conditions our team of 5 flame artists, 10 CG artists, 2 nuke
artists, and 10 roto artists delivered 80 shots of varying complexity in under
two and a half weeks from 2K flat plates, some shots reaching completion in
the final minutes before delivery.