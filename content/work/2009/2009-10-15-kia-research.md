---
title: Kia Research
subtitle:
slug: kia-research
date: 2009-10-15
role:
headline: directed by Gerard De Thame, Believe Media for David + Goliath
summary: Researches have concluded that 7 is bigger than 2.
excerpt: Researches have concluded that 7 is bigger than 2.
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- VFX
- compositing
- photoreal
- set extensions
albums:
-   cover:
    images:
    -   caption: ''
        filename: kia-research.01.jpg
    -   caption: ''
        filename: kia-research.02.jpg
    -   caption: ''
        filename: kia-research.03.jpg
    -   caption: ''
        filename: kia-research.04.jpg
    -   caption: ''
        filename: kia-research.05.jpg
    -   caption: ''
        filename: kia-research.06.jpg
    path: work/2009/kia-research/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/kia-research/videos/kia-research.jpg
    path: work/2009/kia-research/videos/kia-research
    slug: kia-research
    sources:
    -   filename: kia-research-1280x720.mp4
        format: mp4
        height: '720'
        size: 20730417
        width: '1280'
    -   filename: kia-research-960x540.mp4
        format: mp4
        height: '540'
        size: 18593229
        width: '960'
    -   filename: kia-research-640x360.mp4
        format: mp4
        height: '360'
        size: 10729713
        width: '640'
    -   filename: kia-research-640x360.ogv
        format: ogv
        height: '360'
        size: 10508012
        width: '640'
    -   filename: kia-research-640x360.webm
        format: webm
        height: '360'
        size: 10429836
        width: '640'
    -   filename: kia-research-480x270.mp4
        format: mp4
        height: '270'
        size: 7731871
        width: '480'
    title: Kia Research
credits:
-   companyName: Kia
    companySlug: kia
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: David + Goliath
    companySlug: david-goliath
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   companyName: Believe Media
    companySlug: believe-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Gerard de Thame
    personSlug: gerard-de-thame
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Researches have concluded that 7 is bigger than 2.