---
title: Verizon Droid Stealth
subtitle:
slug: verizon-droid-stealth
date: 2009-09-03
role:
headline: directed by Rupert Sanders, MJZ
summary: A fleet of stealth planes drops Verizon's new Droid phone to an unsuspectingAmerica.
excerpt: A fleet of stealth planes drops Verizon's new Droid phone to an unsuspectingAmerica.
published: false
featured: false
categories:
- Advertising
tags:
- cellular
- VFX
- compositing
- photoreal
- effects
- CG
- aircraft
albums:
-   cover:
    images:
    -   caption: ''
        filename: verizon-droid-stealth.01.jpg
    -   caption: ''
        filename: verizon-droid-stealth.02.jpg
    -   caption: ''
        filename: verizon-droid-stealth.03.jpg
    -   caption: ''
        filename: verizon-droid-stealth.04.jpg
    -   caption: ''
        filename: verizon-droid-stealth.05.jpg
    -   caption: ''
        filename: verizon-droid-stealth.06.jpg
    -   caption: ''
        filename: verizon-droid-stealth.07.jpg
    -   caption: ''
        filename: verizon-droid-stealth.08.jpg
    -   caption: ''
        filename: verizon-droid-stealth.09.jpg
    path: work/2009/verizon-droid-stealth/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/verizon-droid-stealth/videos/verizon-droid-stealth.jpg
    path: work/2009/verizon-droid-stealth/videos/verizon-droid-stealth
    slug: verizon-droid-stealth
    sources:
    -   filename: verizon-droid-stealth-1280x720.mp4
        format: mp4
        height: '720'
        size: 21110969
        width: '1280'
    -   filename: verizon-droid-stealth-960x540.mp4
        format: mp4
        height: '540'
        size: 13458202
        width: '960'
    -   filename: verizon-droid-stealth-640x360.mp4
        format: mp4
        height: '360'
        size: 7645222
        width: '640'
    -   filename: verizon-droid-stealth-640x360.ogv
        format: ogv
        height: '360'
        size: 7335054
        width: '640'
    -   filename: verizon-droid-stealth-640x360.webm
        format: webm
        height: '360'
        size: 5102368
        width: '640'
    -   filename: verizon-droid-stealth-480x270.mp4
        format: mp4
        height: '270'
        size: 4659920
        width: '480'
    title: Verizon Droid Stealth
credits:
-   companyName: Verizon
    companySlug: verizon
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Rupert Sanders
    personSlug: rupert-sanders
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Jens Zalzala
    personSlug: jens-zalzala
    roleSlug: cg-sup
    roleTitle: CG Supervisor
    visible: false
---
In this spot a fleet of sleek stealth aircraft drop Verizon's new Droid phone
across America, a futuristic almost alien device that seems to have an
intelligence of it's own. The spot draws on science fiction motifs as much as
suspenseful Hitchcock-esque storytelling ones. Directed by MJZ's Rupert
Sanders, I supervised the extensive visual effects which included a fully CGI
sequence of stealth planes dropping their high-tech packages from high
altitude, multiple pod trails and impact explosions, and sky replacements
throughout. We shot over four days in the mountains and desert around
Palmdale, CA in blistering heat and dust storms. We started the process early
on working in close collaboration with Rupert's production design team to
design the aesthetic of the plane and droid 'pods'. Previsualization was built
as a combination of animated CGI and footage taken from multiple sources. The
editorial goal was always to build a montage that felt like it was cut from a
large selection of dailies as opposed to a meticulous choreographed and
correspondingly unnatural sequence. Whenever we felt that it was feeling too
fluid, composition or shot selection was adjusted to give it a more
naturalistic energy. All the live action explosions, and trails were
composited in Flame. CGI planes and terrain were created in wide array of
packages (Maya, Terragen, Renderman, BodyPaint, etc.) and composited in Nuke
using multiple floating-point, linear light passes. The plane was built from
the ground up combining diffuse, specular, reflection, fresnel, and ambient
occlusion passes and additional lighting tweaks and motion blur were added
using normal passes and motion vector passes. All of this was packaged into a
custom interface that controlled the comp process and guaranteed a consistent
look from shot to shot. Additional custom UI tools were created in Nuke to
create the lens flare and lighting effects that incorporate realistic camera
motion, reflections, and light leaks.