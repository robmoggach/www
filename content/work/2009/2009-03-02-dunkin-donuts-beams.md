---
title: Dunkin Donuts Beams
subtitle:
slug: dunkin-donuts-beams
date: 2009-03-02
role:
headline: directed by Rocky Morton, MJZ
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- food
- VFX
- compositing
- photoreal
- projection
albums:
-   cover:
    images:
    -   caption: ''
        filename: dunkin-donuts-beams.01.jpg
    -   caption: ''
        filename: dunkin-donuts-beams.02.jpg
    -   caption: ''
        filename: dunkin-donuts-beams.03.jpg
    -   caption: ''
        filename: dunkin-donuts-beams.04.jpg
    path: work/2009/dunkin-donuts-beams/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/dunkin-donuts-beams/videos/dunkin-donuts-beams.jpg
    path: work/2009/dunkin-donuts-beams/videos/dunkin-donuts-beams
    slug: dunkin-donuts-beams
    sources:
    -   filename: dunkin-donuts-beams-1280x720.mp4
        format: mp4
        height: '720'
        size: 8964636
        width: '1280'
    -   filename: dunkin-donuts-beams-960x540.mp4
        format: mp4
        height: '540'
        size: 5565382
        width: '960'
    -   filename: dunkin-donuts-beams-640x360.mp4
        format: mp4
        height: '360'
        size: 3179096
        width: '640'
    -   filename: dunkin-donuts-beams-640x360.ogv
        format: ogv
        height: '360'
        size: 3116850
        width: '640'
    -   filename: dunkin-donuts-beams-640x360.webm
        format: webm
        height: '360'
        size: 2454565
        width: '640'
    -   filename: dunkin-donuts-beams-480x270.mp4
        format: mp4
        height: '270'
        size: 2012189
        width: '480'
    title: Dunkin Donuts Beams
credits:
-   companyName: Dunkin Donuts
    companySlug: dunkin-donuts
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Rocky Morton
    personSlug: rocky-morton
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
