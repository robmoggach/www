---
title: Transformers
subtitle:
slug: transformers
date: 2007-07-02
role:
headline: directed by Michael Bay, Dreamworks
summary:
excerpt:
published: true
featured: true
categories:
    - Feature Films
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: transformers.01.jpg
          - caption: ''
            filename: transformers.02.jpg
          - caption: ''
            filename: transformers.03.jpg
          - caption: ''
            filename: transformers.04.jpg
          - caption: ''
            filename: transformers.05.jpg
          - caption: ''
            filename: transformers.06.jpg
      path: work/2007/transformers/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2007/transformers/videos/transformers.jpg
      path: work/2007/transformers/videos/transformers
      slug: transformers
      sources:
          - filename: transformers-1280x720.mp4
            format: mp4
            height: '720'
            size: 11171404
            width: '1280'
          - filename: transformers-960x540.mp4
            format: mp4
            height: '540'
            size: 8352059
            width: '960'
          - filename: transformers-640x360.ogv
            format: ogv
            height: '360'
            size: 5481304
            width: '640'
          - filename: transformers-640x360.mp4
            format: mp4
            height: '360'
            size: 5280236
            width: '640'
          - filename: transformers-640x360.webm
            format: webm
            height: '360'
            size: 4311303
            width: '640'
          - filename: transformers-480x270.mp4
            format: mp4
            height: '270'
            size: 3668566
            width: '480'
      title: Transformers
credits:
    - companyName: Dreamworks
      companySlug: dreamworks
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: Michael Bay
      personSlug: michael-bay
      roleSlug: director
      roleTitle: Director
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: lead flame
      roleTitle: Lead Flame
      visible: false
---
