---
title: Pontiac Pulse
subtitle:
slug: pontiac-pulse
date: 2007-04-01
role:
headline: directed by Toby Tremlett, Plum Productions
summary: New Pontiac cars race through a black void creating a beautiful graphiclandscape
    of light.
excerpt: New Pontiac cars race through a black void creating a beautiful graphiclandscape
    of light.
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- projection
- design
- animation
- CG
albums:
-   cover:
    images:
    -   caption: ''
        filename: pontiac-pulse-g6-g5.01.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-g5.02.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-g5.03.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-g5.04.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-g5.05.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-g5.06.jpg
    path: work/2007/pontiac-pulse/images/pontiac-pulse-g6-g5-stills
    slug: pontiac-pulse-g6-g5-stills
    title: Pontiac Pulse G6 G5 Stills
-   cover:
    images:
    -   caption: ''
        filename: pontiac-pulse-g6-series.01.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-series.02.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-series.03.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-series.04.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-series.05.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-series.06.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-series.07.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-series.08.jpg
    path: work/2007/pontiac-pulse/images/pontiac-pulse-g6-series-stills
    slug: pontiac-pulse-g6-series-stills
    title: Pontiac Pulse G6 Series Stills
-   cover:
    images:
    -   caption: ''
        filename: pontiac-pulse-g6-torrent.02.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-torrent.03.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-torrent.04.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-torrent.05.jpg
    -   caption: ''
        filename: pontiac-pulse-g6-torrent.06.jpg
    path: work/2007/pontiac-pulse/images/pontiac-pulse-g6-torrent-stills
    slug: pontiac-pulse-g6-torrent-stills
    title: Pontiac Pulse G6 Torrent Stills
-   cover:
    images:
    -   caption: ''
        filename: pontiac-pulse-vibe.01.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.02.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.03.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.04.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.05.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.06.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.07.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.08.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.09.jpg
    -   caption: ''
        filename: pontiac-pulse-vibe.10.jpg
    path: work/2007/pontiac-pulse/images/pontiac-pulse-vibe-stills
    slug: pontiac-pulse-vibe-stills
    title: Pontiac Pulse Vibe Stills
videos:
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-g5.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-g5
    slug: pontiac-pulse-g6-g5
    sources:
    -   filename: pontiac-pulse-g6-g5-960x540.mp4
        format: mp4
        height: '540'
        size: 11498674
        width: '960'
    -   filename: pontiac-pulse-g6-g5-1280x720.mp4
        format: mp4
        height: '720'
        size: 10644894
        width: '1280'
    -   filename: pontiac-pulse-g6-g5-480x270.mp4
        format: mp4
        height: '270'
        size: 5366757
        width: '480'
    -   filename: pontiac-pulse-g6-g5-640x360.mp4
        format: mp4
        height: '360'
        size: 5324558
        width: '640'
    -   filename: pontiac-pulse-g6-g5-640x360.webm
        format: webm
        height: '360'
        size: 5216390
        width: '640'
    -   filename: pontiac-pulse-g6-g5-640x360.ogv
        format: ogv
        height: '360'
        size: 5191722
        width: '640'
    title: Pontiac Pulse G6 G5
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-g5-4x3.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-g5-4x3
    slug: pontiac-pulse-g6-g5-4x3
    sources:
    -   filename: pontiac-pulse-g6-g5.960x720.mp4
        format: mp4
        height: '720'
        size: 11498674
        width: '960'
    -   filename: pontiac-pulse-g6-g5.1280x960.mp4
        format: mp4
        height: '960'
        size: 10644894
        width: '1280'
    -   filename: pontiac-pulse-g6-g5.480x360.mp4
        format: mp4
        height: '360'
        size: 5366757
        width: '480'
    -   filename: pontiac-pulse-g6-g5.640x480.mp4
        format: mp4
        height: '480'
        size: 5324558
        width: '640'
    -   filename: pontiac-pulse-g6-g5.640x360.webm
        format: webm
        height: '360'
        size: 5216390
        width: '640'
    -   filename: pontiac-pulse-g6-g5.640x360.ogv
        format: ogv
        height: '360'
        size: 5191722
        width: '640'
    title: Pontiac Pulse G6 G5 4X3
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-series.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-series
    slug: pontiac-pulse-g6-series
    sources:
    -   filename: pontiac-pulse-g6-series-1280x720.mp4
        format: mp4
        height: '720'
        size: 10615619
        width: '1280'
    -   filename: pontiac-pulse-g6-series-960x540.mp4
        format: mp4
        height: '540'
        size: 8348670
        width: '960'
    -   filename: pontiac-pulse-g6-series-640x360.mp4
        format: mp4
        height: '360'
        size: 5347381
        width: '640'
    -   filename: pontiac-pulse-g6-series-640x360.ogv
        format: ogv
        height: '360'
        size: 5237972
        width: '640'
    -   filename: pontiac-pulse-g6-series-640x360.webm
        format: webm
        height: '360'
        size: 4665769
        width: '640'
    -   filename: pontiac-pulse-g6-series-480x270.mp4
        format: mp4
        height: '270'
        size: 3557068
        width: '480'
    title: Pontiac Pulse G6 Series
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-series-4x3.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-series-4x3
    slug: pontiac-pulse-g6-series-4x3
    sources:
    -   filename: pontiac-pulse-g6-series.1280x960.mp4
        format: mp4
        height: '960'
        size: 10615619
        width: '1280'
    -   filename: pontiac-pulse-g6-series.960x720.mp4
        format: mp4
        height: '720'
        size: 8348670
        width: '960'
    -   filename: pontiac-pulse-g6-series.640x480.mp4
        format: mp4
        height: '480'
        size: 5347381
        width: '640'
    -   filename: pontiac-pulse-g6-series.640x360.ogv
        format: ogv
        height: '360'
        size: 5237972
        width: '640'
    -   filename: pontiac-pulse-g6-series.640x360.webm
        format: webm
        height: '360'
        size: 4665769
        width: '640'
    -   filename: pontiac-pulse-g6-series.480x360.mp4
        format: mp4
        height: '360'
        size: 3557068
        width: '480'
    title: Pontiac Pulse G6 Series 4X3
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-torrent.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-torrent
    slug: pontiac-pulse-g6-torrent
    sources:
    -   filename: pontiac-pulse-g6-torrent-1280x720.mp4
        format: mp4
        height: '720'
        size: 10588617
        width: '1280'
    -   filename: pontiac-pulse-g6-torrent-960x540.mp4
        format: mp4
        height: '540'
        size: 8401130
        width: '960'
    -   filename: pontiac-pulse-g6-torrent-640x360.mp4
        format: mp4
        height: '360'
        size: 5320657
        width: '640'
    -   filename: pontiac-pulse-g6-torrent-640x360.ogv
        format: ogv
        height: '360'
        size: 5222080
        width: '640'
    -   filename: pontiac-pulse-g6-torrent-640x360.webm
        format: webm
        height: '360'
        size: 5126969
        width: '640'
    -   filename: pontiac-pulse-g6-torrent-480x270.mp4
        format: mp4
        height: '270'
        size: 3853070
        width: '480'
    title: Pontiac Pulse G6 Torrent
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-torrent-4x3.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-g6-torrent-4x3
    slug: pontiac-pulse-g6-torrent-4x3
    sources:
    -   filename: pontiac-pulse-g6-torrent.1280x960.mp4
        format: mp4
        height: '960'
        size: 10588617
        width: '1280'
    -   filename: pontiac-pulse-g6-torrent.960x720.mp4
        format: mp4
        height: '720'
        size: 8401130
        width: '960'
    -   filename: pontiac-pulse-g6-torrent.640x480.mp4
        format: mp4
        height: '480'
        size: 5320657
        width: '640'
    -   filename: pontiac-pulse-g6-torrent.640x360.ogv
        format: ogv
        height: '360'
        size: 5222080
        width: '640'
    -   filename: pontiac-pulse-g6-torrent.640x360.webm
        format: webm
        height: '360'
        size: 5126969
        width: '640'
    -   filename: pontiac-pulse-g6-torrent.480x360.mp4
        format: mp4
        height: '360'
        size: 3853070
        width: '480'
    title: Pontiac Pulse G6 Torrent 4X3
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-vibe.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-vibe
    slug: pontiac-pulse-vibe
    sources:
    -   filename: pontiac-pulse-vibe-1280x720.mp4
        format: mp4
        height: '720'
        size: 10482160
        width: '1280'
    -   filename: pontiac-pulse-vibe-960x540.mp4
        format: mp4
        height: '540'
        size: 8966127
        width: '960'
    -   filename: pontiac-pulse-vibe-640x360.mp4
        format: mp4
        height: '360'
        size: 5343855
        width: '640'
    -   filename: pontiac-pulse-vibe-640x360.ogv
        format: ogv
        height: '360'
        size: 5230980
        width: '640'
    -   filename: pontiac-pulse-vibe-640x360.webm
        format: webm
        height: '360'
        size: 5166256
        width: '640'
    -   filename: pontiac-pulse-vibe-480x270.mp4
        format: mp4
        height: '270'
        size: 4094473
        width: '480'
    title: Pontiac Pulse Vibe
-   caption: ''
    cover: work/2007/pontiac-pulse/videos/pontiac-pulse-vibe-4x3.jpg
    path: work/2007/pontiac-pulse/videos/pontiac-pulse-vibe-4x3
    slug: pontiac-pulse-vibe-4x3
    sources:
    -   filename: pontiac-pulse-vibe.1280x960.mp4
        format: mp4
        height: '960'
        size: 10482160
        width: '1280'
    -   filename: pontiac-pulse-vibe.960x720.mp4
        format: mp4
        height: '720'
        size: 8966127
        width: '960'
    -   filename: pontiac-pulse-vibe.640x480.mp4
        format: mp4
        height: '480'
        size: 5343855
        width: '640'
    -   filename: pontiac-pulse-vibe.640x360.ogv
        format: ogv
        height: '360'
        size: 5230980
        width: '640'
    -   filename: pontiac-pulse-vibe.640x360.webm
        format: webm
        height: '360'
        size: 5166256
        width: '640'
    -   filename: pontiac-pulse-vibe.480x360.mp4
        format: mp4
        height: '360'
        size: 4094473
        width: '480'
    title: Pontiac Pulse Vibe 4X3
credits:
-   companyName: Pontiac
    companySlug: pontiac
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Plum Productions
    companySlug: plum-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Toby Tremlett
    personSlug: toby-tremlett
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   roleSlug: director
    roleTitle: Director
    visible: false
---
New Pontiac cars race through a black void creating a beautiful graphic
landscape of light.