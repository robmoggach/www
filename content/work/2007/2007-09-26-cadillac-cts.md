---
title: Cadillac Cts
subtitle:
slug: cadillac-cts
date: 2007-09-26
role:
headline: directed by Joe Carnahan, RSA Films
summary: "Cadillacs power through the night leaving a blur of graphic streaks in their\r\
    wake."
excerpt: "Cadillacs power through the night leaving a blur of graphic streaks in their\r\
    wake."
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- projection
- compositing
- colour
albums:
-   cover:
    images:
    -   caption: ''
        filename: cadillac-cts-ali.01.jpg
    -   caption: ''
        filename: cadillac-cts-ali.02.jpg
    -   caption: ''
        filename: cadillac-cts-ali.03.jpg
    -   caption: ''
        filename: cadillac-cts-ali.04.jpg
    -   caption: ''
        filename: cadillac-cts-ali.05.jpg
    -   caption: ''
        filename: cadillac-cts-ali.06.jpg
    -   caption: ''
        filename: cadillac-cts-ali.07.jpg
    -   caption: ''
        filename: cadillac-cts-ali.08.jpg
    -   caption: ''
        filename: cadillac-cts-ali.09.jpg
    -   caption: ''
        filename: cadillac-cts-ali.10.jpg
    path: work/2007/cadillac-cts/images/cadillac-cts-ali-stills
    slug: cadillac-cts-ali-stills
    title: Cadillac Cts Ali Stills
-   cover:
    images:
    -   caption: ''
        filename: cadillac-cts-ftf.01.jpg
    path: work/2007/cadillac-cts/images/ftf
    slug: ftf
    title: Ftf
-   cover:
    images:
    -   caption: ''
        filename: cadillac-cts-hammer.01.jpg
    path: work/2007/cadillac-cts/images/hammer
    slug: hammer
    title: Hammer
-   cover:
    images:
    -   caption: ''
        filename: cadillac-cts-tyok.01.jpg
    path: work/2007/cadillac-cts/images/tyok
    slug: tyok
    title: Tyok
-   cover:
    images:
    -   caption: ''
        filename: cadillac-cts-tyom.01.jpg
    path: work/2007/cadillac-cts/images/tyom
    slug: tyom
    title: Tyom
videos:
-   caption: ''
    cover: work/2007/cadillac-cts/videos/cadillac-cts-ali.jpg
    path: work/2007/cadillac-cts/videos/cadillac-cts-ali
    slug: cadillac-cts-ali
    sources:
    -   filename: cadillac-cts-ali-1280x720.mp4
        format: mp4
        height: '720'
        size: 10455603
        width: '1280'
    -   filename: cadillac-cts-ali-960x540.mp4
        format: mp4
        height: '540'
        size: 8768245
        width: '960'
    -   filename: cadillac-cts-ali-640x360.mp4
        format: mp4
        height: '360'
        size: 5312763
        width: '640'
    -   filename: cadillac-cts-ali-640x360.ogv
        format: ogv
        height: '360'
        size: 5269053
        width: '640'
    -   filename: cadillac-cts-ali-640x360.webm
        format: webm
        height: '360'
        size: 4525136
        width: '640'
    -   filename: cadillac-cts-ali-480x270.mp4
        format: mp4
        height: '270'
        size: 3606810
        width: '480'
    title: Cadillac Cts Ali
-   caption: ''
    cover: work/2007/cadillac-cts/videos/cadillac-cts-favorite-things.jpg
    path: work/2007/cadillac-cts/videos/cadillac-cts-favorite-things
    slug: cadillac-cts-favorite-things
    sources:
    -   filename: cadillac-cts-ftf-1280x720.mp4
        format: mp4
        height: '720'
        size: 10543415
        width: '1280'
    -   filename: cadillac-cts-ftf-960x540.mp4
        format: mp4
        height: '540'
        size: 7347947
        width: '960'
    -   filename: cadillac-cts-ftf-640x360.mp4
        format: mp4
        height: '360'
        size: 4696882
        width: '640'
    -   filename: cadillac-cts-ftf-640x360.ogv
        format: ogv
        height: '360'
        size: 4589227
        width: '640'
    -   filename: cadillac-cts-ftf-640x360.webm
        format: webm
        height: '360'
        size: 3635290
        width: '640'
    -   filename: cadillac-cts-ftf-480x270.mp4
        format: mp4
        height: '270'
        size: 2916350
        width: '480'
    title: Cadillac Cts Favorite Things
-   caption: ''
    cover: work/2007/cadillac-cts/videos/cadillac-cts-hammer.jpg
    path: work/2007/cadillac-cts/videos/cadillac-cts-hammer
    slug: cadillac-cts-hammer
    sources:
    -   filename: cadillac-cts-hammer-1280x720.mp4
        format: mp4
        height: '720'
        size: 10511628
        width: '1280'
    -   filename: cadillac-cts-hammer-960x540.mp4
        format: mp4
        height: '540'
        size: 9887414
        width: '960'
    -   filename: cadillac-cts-hammer-640x360.mp4
        format: mp4
        height: '360'
        size: 5351559
        width: '640'
    -   filename: cadillac-cts-hammer-640x360.ogv
        format: ogv
        height: '360'
        size: 5262146
        width: '640'
    -   filename: cadillac-cts-hammer-640x360.webm
        format: webm
        height: '360'
        size: 4995034
        width: '640'
    -   filename: cadillac-cts-hammer-480x270.mp4
        format: mp4
        height: '270'
        size: 3868636
        width: '480'
    title: Cadillac Cts Hammer
-   caption: ''
    cover: work/2007/cadillac-cts/videos/cadillac-cts-turn-you-on-k.jpg
    path: work/2007/cadillac-cts/videos/cadillac-cts-turn-you-on-k
    slug: cadillac-cts-turn-you-on-k
    sources:
    -   filename: cadillac-cts-tyok-1280x720.mp4
        format: mp4
        height: '720'
        size: 10591368
        width: '1280'
    -   filename: cadillac-cts-tyok-960x540.mp4
        format: mp4
        height: '540'
        size: 8164425
        width: '960'
    -   filename: cadillac-cts-tyok-640x360.mp4
        format: mp4
        height: '360'
        size: 5237145
        width: '640'
    -   filename: cadillac-cts-tyok-640x360.ogv
        format: ogv
        height: '360'
        size: 4752359
        width: '640'
    -   filename: cadillac-cts-tyok-640x360.webm
        format: webm
        height: '360'
        size: 3943057
        width: '640'
    -   filename: cadillac-cts-tyok-480x270.mp4
        format: mp4
        height: '270'
        size: 3265496
        width: '480'
    title: Cadillac Cts Turn You On K
-   caption: ''
    cover: work/2007/cadillac-cts/videos/cadillac-cts-turn-you-on-m.jpg
    path: work/2007/cadillac-cts/videos/cadillac-cts-turn-you-on-m
    slug: cadillac-cts-turn-you-on-m
    sources:
    -   filename: cadillac-cts-tyom-1280x720.mp4
        format: mp4
        height: '720'
        size: 10486529
        width: '1280'
    -   filename: cadillac-cts-tyom-960x540.mp4
        format: mp4
        height: '540'
        size: 9170075
        width: '960'
    -   filename: cadillac-cts-tyom-640x360.mp4
        format: mp4
        height: '360'
        size: 5319297
        width: '640'
    -   filename: cadillac-cts-tyom-640x360.ogv
        format: ogv
        height: '360'
        size: 5266004
        width: '640'
    -   filename: cadillac-cts-tyom-640x360.webm
        format: webm
        height: '360'
        size: 4772041
        width: '640'
    -   filename: cadillac-cts-tyom-480x270.mp4
        format: mp4
        height: '270'
        size: 3684844
        width: '480'
    title: Cadillac Cts Turn You On M
credits:
-   companyName: Cadillac
    companySlug: cadillac
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Joe Carnahan
    personSlug: joe-carnahan
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Paul Cameron
    personSlug: paul-cameron
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
Cadillacs power through the night leaving a blur of graphic streaks in their
wake.