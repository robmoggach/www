---
title: Propel Stress Monster
subtitle:
slug: propel-stress-monster
date: 2007-03-01
role:
headline: directed by Baker Smith, Harvest
summary: Giant monster made of stressful objects runs through San Francisco.
excerpt: Giant monster made of stressful objects runs through San Francisco.
published: false
featured: false
categories:
- Advertising
tags:
- animation
- characters
- compositing
- effects
- food
- photoreal
- tracking
albums:
-   cover:
    images:
    -   caption: ''
        filename: propel-stress-monster.01.jpg
    -   caption: ''
        filename: propel-stress-monster.02.jpg
    -   caption: ''
        filename: propel-stress-monster.03.jpg
    -   caption: ''
        filename: propel-stress-monster.04.jpg
    -   caption: ''
        filename: propel-stress-monster.05.jpg
    -   caption: ''
        filename: propel-stress-monster.06.jpg
    -   caption: ''
        filename: propel-stress-monster.07.jpg
    path: work/2007/propel-stress-monster/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2007/propel-stress-monster/videos/propel-stress-monster.jpg
    path: work/2007/propel-stress-monster/videos/propel-stress-monster
    slug: propel-stress-monster
    sources:
    -   filename: propel-stress-monster-960x540.mp4
        format: mp4
        height: '540'
        size: 21509742
        width: '960'
    -   filename: propel-stress-monster-1280x720.mp4
        format: mp4
        height: '720'
        size: 20916792
        width: '1280'
    -   filename: propel-stress-monster-640x360.mp4
        format: mp4
        height: '360'
        size: 10562294
        width: '640'
    -   filename: propel-stress-monster-640x360.ogv
        format: ogv
        height: '360'
        size: 10488572
        width: '640'
    -   filename: propel-stress-monster-640x360.webm
        format: webm
        height: '360'
        size: 9979383
        width: '640'
    -   filename: propel-stress-monster-480x270.mp4
        format: mp4
        height: '270'
        size: 7567839
        width: '480'
    title: Propel Stress Monster
credits:
-   companyName: Propel
    companySlug: propel
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Harvest Films
    companySlug: harvest-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Baker Smith
    personSlug: baker-smith
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Sean Faden
    personSlug: sean-faden
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: dfx-supervisor
    roleTitle: DFX Supervisor
    visible: false
---
Giant monster made of stressful objects runs through San Francisco.