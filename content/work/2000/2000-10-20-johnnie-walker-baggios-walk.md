---
title: Johnnie Walker | "Baggio's Walk"
subtitle:
slug: johnnie-walker-baggios-walk
date: 2000-10-20
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - Compositing
    - Flame
    - crowds
    - matte painting
albums:
    - cover:
      images:
          - caption: ''
            filename: johnnie-walker-baggios-walk.01.jpg
          - caption: ''
            filename: johnnie-walker-baggios-walk.02.jpg
          - caption: ''
            filename: johnnie-walker-baggios-walk.03.jpg
          - caption: ''
            filename: johnnie-walker-baggios-walk.04.jpg
          - caption: ''
            filename: johnnie-walker-baggios-walk.05.jpg
          - caption: ''
            filename: johnnie-walker-baggios-walk.06.jpg
          - caption: ''
            filename: johnnie-walker-baggios-walk.07.jpg
      path: work/2000/johnnie-walker-baggios-walk/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2000/johnnie-walker-baggios-walk/videos/johnnie-walker-baggios-walk.jpg
      path: work/2000/johnnie-walker-baggios-walk/videos/johnnie-walker-baggios-walk
      slug: johnnie-walker-baggios-walk
      sources:
          - filename: johnnie-walker-baggios-walk.1280x960.mp4
            format: mp4
            height: '960'
            size: 20929032
            width: '1280'
          - filename: johnnie-walker-baggios-walk.960x720.mp4
            format: mp4
            height: '720'
            size: 20156704
            width: '960'
          - filename: johnnie-walker-baggios-walk.640x480.mp4
            format: mp4
            height: '480'
            size: 10699157
            width: '640'
          - filename: johnnie-walker-baggios-walk.640x360.ogv
            format: ogv
            height: '360'
            size: 10489547
            width: '640'
          - filename: johnnie-walker-baggios-walk.640x360.webm
            format: webm
            height: '360'
            size: 10435873
            width: '640'
          - filename: johnnie-walker-baggios-walk.480x360.mp4
            format: mp4
            height: '360'
            size: 8892186
            width: '480'
      title: Johnnie Walker Baggios Walk
credits:
    - companyName: Bartle Bogle Hegarty
      companySlug: bbh
      roleSlug: agency
      roleTitle: Advertising Agency
      visible: true
    - companyName: Bartle Bogle Hegarty
      companySlug: bbh
      personName: Steve Elrick
      personSlug: steve-elrick
      roleSlug: agency-cd
      roleTitle: Creative Director
      visible: true
    - companyName: Bartle Bogle Hegarty
      companySlug: bbh
      personName: Tony Williams
      personSlug: tony-williams
      roleSlug: agency-art-director
      roleTitle: Art Director
      visible: true
    - companyName: Smoke & Mirrors
      companySlug: smoke-mirrors
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: Smoke & Mirrors
      companySlug: smoke-mirrors
      personName: Judy Roberts
      personSlug: judy-roberts
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
    - companyName: Smoke & Mirrors
      companySlug: smoke-mirrors
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
    - personName: Jason Schragger
      personSlug: jason-schragger
      roleSlug: agency-copywriter
      roleTitle: Copywriter
      visible: true
---

In 2000, Johnnie Walker launched a campaign depicting the emotional â€šÃ„Ã²walkâ€šÃ„Ã´
that has characterized the lives of select famous figures. Baggio, an Italian
soccer / football player, while in a shoot out for the 1994 World Cup, missed
the goal, losing the game. Four years later he had a second chance against the
same team. This commercial depicts his walk on to the field, full of anxiety
from being given a second chance. Shot in a empty stadium, we had the task of
filling the stadium with an excited crowd typical of World Cup games. To add
to the complexity of this commercial, it was shot with a variety of moving
shots, that required massive amounts of precise tracking of crowd elements.
All shots were filmed with an empty stadium that was populated using crowd
footage shot of 200 extras, particles generated in inferno, and painted
textures. Shots with extreme rotations were â€šÃ„Ã²unwrappedâ€šÃ„Ã´ and recreated as
oversized tiles in Photoshop and tracked & warped into position. In addition
to the crowds that needed to be added to almost every shot, the director felt
that in many shots, the players needed to be repositioned for better
composition. Players were cut out in entirety and moved to create the new
composition. In certain cases players were flipped and required their numbers
to remain in the correct orientation.
