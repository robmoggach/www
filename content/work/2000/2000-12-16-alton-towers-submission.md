---
title: Alton Towers "Submission"
subtitle:
slug: alton-towers-submission
date: 2000-12-16
role:
headline: ''
summary:
excerpt:
published: true
featured: false
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: alton-towers-submission.01.jpg
          - caption: ''
            filename: alton-towers-submission.02.jpg
          - caption: ''
            filename: alton-towers-submission.03.jpg
      path: work/2000/alton-towers-submission/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2000/alton-towers-submission/videos/alton-towers-submission.jpg
      path: work/2000/alton-towers-submission/videos/alton-towers-submission
      slug: alton-towers-submission
      sources:
          - filename: alton-towers-submission.1280x720.mp4
            format: mp4
            height: '720'
            size: 3445655
            width: '1280'
          - filename: alton-towers-submission.960x540.mp4
            format: mp4
            height: '540'
            size: 2820382
            width: '960'
          - filename: alton-towers-submission.640x360.mp4
            format: mp4
            height: '360'
            size: 1731242
            width: '640'
          - filename: alton-towers-submission.640x360.ogv
            format: ogv
            height: '360'
            size: 1195171
            width: '640'
          - filename: alton-towers-submission.640x360.webm
            format: webm
            height: '360'
            size: 878743
            width: '640'
          - filename: alton-towers-submission.480x270.mp4
            format: mp4
            height: '270'
            size: 845083
            width: '480'
      title: Alton Towers Submission
credits: []
---

The Alton Towers amusement park in the united kingdom has always created
visually stunning ads that serve to promote it's new rides for the year. In
2001, they introduced the "Submission" ride and celebrated the 25th
anniversary of their "Corkscrew" ride. The commercial for the "Submission"
ride was a combination of mostly CGI elements, composited in inferno.
Additional green screen elements and vector artwork were also involved. The
CGI was heavily treated with a variety of focus pulls, grading and lens
effects. The product shot of the amusement park ride involved creating a
"time-slice" look without using "time-slice" by shooting five angles of the
ride in motion at high speed and using a proprietary process of morphing
between them. The commercial for the "Corkscrew" ride was a simple yet elegant
combination of multiple elements shot in camera. The various elements were
first cleaned of any imperfections and then combined using a variety of mixes
and layering to create a slow, elegant, dream sequence celebrating the ride's
twenty one years of inducing nausea! My job was to combine the elements in a
way that was fluid and smooth without detracting from the beauty of the shot
footage.
