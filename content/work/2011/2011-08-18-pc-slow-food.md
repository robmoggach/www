---
title: Pc Slow Food
subtitle:
slug: pc-slow-food
date: 2011-08-18
role:
headline: directed by John McDougall, Ruckus Films for Bensimon Byrne
summary: The ballet that is slow motion baby food being flung around a kitchen by
    someenergetic toddlers was the subject for this spot. Involved from early in thecreative
    process, Dashing was tasked with not only making the food look greatbut grading
    and finishing the spot in it's entirety.
excerpt: The ballet that is slow motion baby food being flung around a kitchen by
    someenergetic toddlers was the subject for this spot. Involved from early in thecreative
    process, Dashing was tasked with not only making the food look greatbut grading
    and finishing the spot in it's entirety.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- colour
- food
albums:
-   cover:
    images:
    -   caption: ''
        filename: pc-slow-food.01.jpg
    -   caption: ''
        filename: pc-slow-food.02.jpg
    -   caption: ''
        filename: pc-slow-food.03.jpg
    -   caption: ''
        filename: pc-slow-food.04.jpg
    -   caption: ''
        filename: pc-slow-food.05.jpg
    -   caption: ''
        filename: pc-slow-food.06.jpg
    path: work/2011/pc-slow-food/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2011/pc-slow-food/videos/pc-slow-food.jpg
    path: work/2011/pc-slow-food/videos/pc-slow-food
    slug: pc-slow-food
    sources:
    -   filename: pc-slow-food-1280x720.mp4
        format: mp4
        height: '720'
        size: 19322759
        width: '1280'
    -   filename: pc-slow-food-960x540.mp4
        format: mp4
        height: '540'
        size: 2123199
        width: '960'
    -   filename: pc-slow-food-640x360.ogv
        format: ogv
        height: '360'
        size: 1664049
        width: '640'
    -   filename: pc-slow-food-640x360.mp4
        format: mp4
        height: '360'
        size: 1613712
        width: '640'
    -   filename: pc-slow-food-640x360.webm
        format: webm
        height: '360'
        size: 1577183
        width: '640'
    -   filename: pc-slow-food-480x270.mp4
        format: mp4
        height: '270'
        size: 1187304
        width: '480'
    title: Pc Slow Food
credits:
-   companyName: President's Choice
    companySlug: presidents-choice
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Bensimon Byrne
    companySlug: bensimon-byrne
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: David Rosenberg
    personSlug: david-rosenberg
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Michelle Pilling
    personSlug: michelle-pilling
    roleSlug: agency-tvhead
    roleTitle: Head of Broadcast
    visible: false
-   companyName: Ruckus Film Company
    companySlug: ruckus-film-company
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: John McDougall
    personSlug: john-mcdougall
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: David Brennan
    personSlug: david-brennan
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Rita Popielak
    personSlug: rita-popielak
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: telecine
    roleTitle: DI / Color
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
---
The ballet that is slow motion baby food being flung around a kitchen by some
energetic toddlers was the subject for this spot. Involved from early in the
creative process, Dashing was tasked with not only making the food look great
but grading and finishing the spot in it's entirety.

Director John MacDougall was really intent on a clean and precise aesthetic
for this film. The contrast of subtle gradations of white and the abstract
forms of the slow motion baby food meant getting the right look required
extensive rotoscoping and selective grading not always possible in one colour
session. Instead Dashing opted for a collaborative approach that saw the
colour of the spot evolve along with the compositing.

> "Having the flexibility to grade in flame, make a few tweaks to the roto or
> compositing work, look at the cut, and then revise all in an integrated
> fashion was a great way to give John full control over the look

Dashing CD Rob Moggach graded/transferred the spot using a combination of
Flame & Nuke and composited the spot using Flame. Rotoscoping of all the
elements throughout meant he was able to selectively add or remove elements
and change composition on the fly.