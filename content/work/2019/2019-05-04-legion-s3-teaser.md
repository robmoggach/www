---
title: Legion | Season 3 "Daisy Chain"
subtitle: Season 3 "Daisy Chain"
slug: legion-s3-teaser
date: 2019-05-04
role: 
headline: ''
summary: 
excerpt: 
published: true
featured: false
categories:
- Advertising
- Television
tags:
- VFX
albums:
- cover: 
  images:
  - caption: ''
    filename: legion-s3-teaser.01.jpg
  - caption: ''
    filename: legion-s3-teaser.02.jpg
  - caption: ''
    filename: legion-s3-teaser.03.jpg
  - caption: ''
    filename: legion-s3-teaser.04.jpg
  - caption: ''
    filename: legion-s3-teaser.05.jpg
  - caption: ''
    filename: legion-s3-teaser.06.jpg
  - caption: ''
    filename: legion-s3-teaser.07.jpg
  - caption: ''
    filename: legion-s3-teaser.08.jpg
  - caption: ''
    filename: legion-s3-teaser.09.jpg
  - caption: ''
    filename: legion-s3-teaser.10.jpg
  path: work/2019/legion-s3-teaser/images/stills
  slug: stills
  title: Stills
videos:
- caption: ''
  cover: work/2019/legion-s3-teaser/videos/legion-s3-teaser/legion-s3-teaser.jpg
  path: work/2019/legion-s3-teaser/videos/legion-s3-teaser
  slug: legion-s3-teaser
  provider: html5
  sources:
  - filename: legion-s3-teaser-1920x1080.mp4
    format: mp4
    height: 1920
    size: 
    width: 1080
  - filename: legion-s3-teaser-1280x720.mp4
    format: mp4
    height: 720
    size: 
    width: 1280
  - filename: legion-s3-teaser-960x540.mp4
    format: mp4
    height: 540
    size: 
    width: 960
  - filename: legion-s3-teaser-480x270.ogv
    format: ogv
    height: 270
    size: 
    width: 480
  - filename: legion-s3-teaser-640x360.mp4
    format: mp4
    height: 360
    size: 
    width: 640
  - filename: legion-s3-teaser-640x360.webm
    format: webm
    height: 360
    size: 
    width: 640
  - filename: legion-s3-teaser-480x270.mp4
    format: mp4
    height: 270
    size: 
    width: 480
  title: Legion Season 3 Teaser "Daisy Chain"
credits:
- companyName: Tendril
  companySlug: tendril
  roleSlug: vfx
  roleTitle: Visual Effects
  visible: true
- companyName: Tendril
  companySlug: tendril
  personName: Kate Bate
  personSlug: kate-bate
  roleSlug: vfx-ep
  roleTitle: Executive Producer
  visible: true
- companyName: Tendril
  companySlug: tendril
  personName: Chris Bahry
  personSlug: chris-bahry
  roleSlug: creative-director
  roleTitle: Creative Director
  visible: true
- companyName: Tendril
  companySlug: tendril
  personName: Robert Moggach
  personSlug: robert-moggach
  roleSlug: vfx-supervisor
  roleTitle: VFX Supervisor
  visible: true

---
A harrowing creative brief is one way to describe this project. With a successful third season in production and a psychedelic theme throughout this project started with the simple notion that we would reveal David's personalities through a psychedelic trip that incorporated many of the abstract elements of the show and it's characters.

Starting with multiple passes of a one-shot, high-speed, varisped, motion-control, celebrity talent on green screen - we were tasked to... ideate and simulate in production to find a visual concept while executing the complex visual effects requirements. The project itself was a visual trip full of explorations into various directions and looking for the next inspiration to continue a search for a style that fit the 60s while being modern, illustrative, psychedelic and realistic all at once.

With such a broad brief and a promo budget, I turned to learning Houdini to develop mandala simulations to feed to the team of artists from around the world. Tendril once again didn't disappoint with great crew and a wonderful culture to work in. Schedule and budget were ambitious and we managed to bring it together to deliver a piece that is unquestionably surreal.