---
title: Toyota Reasons
subtitle:
slug: toyota-reasons
date: 2006-09-01
role:
headline: directed by Gerard De Thame, HSI
summary: Peoples reasons for choosing Hybrid Technology are personal encapsulations.
excerpt: Peoples reasons for choosing Hybrid Technology are personal encapsulations.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- photoreal
- compositing
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: toyota-reasons-forerunner.01.jpg
    -   caption: ''
        filename: toyota-reasons-forerunner.02.jpg
    -   caption: ''
        filename: toyota-reasons-forerunner.03.jpg
    path: work/2006/toyota-reasons/images/toyota-reasons-forerunner-stills
    slug: toyota-reasons-forerunner-stills
    title: Toyota Reasons Forerunner Stills
-   cover:
    images:
    -   caption: ''
        filename: toyota-reasons-prius.01.jpg
    -   caption: ''
        filename: toyota-reasons-prius.02.jpg
    -   caption: ''
        filename: toyota-reasons-prius.03.jpg
    path: work/2006/toyota-reasons/images/toyota-reasons-prius-stills
    slug: toyota-reasons-prius-stills
    title: Toyota Reasons Prius Stills
videos:
-   caption: ''
    cover: work/2006/toyota-reasons/videos/toyota-reasons-forerunner.jpg
    path: work/2006/toyota-reasons/videos/toyota-reasons-forerunner
    slug: toyota-reasons-forerunner
    sources:
    -   filename: toyota-reasons-forerunner-1280x720.mp4
        format: mp4
        height: '720'
        size: 10472158
        width: '1280'
    -   filename: toyota-reasons-forerunner-960x540.mp4
        format: mp4
        height: '540'
        size: 8434455
        width: '960'
    -   filename: toyota-reasons-forerunner-640x360.ogv
        format: ogv
        height: '360'
        size: 5282543
        width: '640'
    -   filename: toyota-reasons-forerunner-640x360.mp4
        format: mp4
        height: '360'
        size: 5107349
        width: '640'
    -   filename: toyota-reasons-forerunner-640x360.webm
        format: webm
        height: '360'
        size: 4124599
        width: '640'
    -   filename: toyota-reasons-forerunner-480x270.mp4
        format: mp4
        height: '270'
        size: 3223082
        width: '480'
    title: Toyota Reasons Forerunner
-   caption: ''
    cover: work/2006/toyota-reasons/videos/toyota-reasons-prius.jpg
    path: work/2006/toyota-reasons/videos/toyota-reasons-prius
    slug: toyota-reasons-prius
    sources:
    -   filename: toyota-reasons-prius-1280x720.mp4
        format: mp4
        height: '720'
        size: 10367223
        width: '1280'
    -   filename: toyota-reasons-prius-960x540.mp4
        format: mp4
        height: '540'
        size: 9202206
        width: '960'
    -   filename: toyota-reasons-prius-640x360.ogv
        format: ogv
        height: '360'
        size: 5273499
        width: '640'
    -   filename: toyota-reasons-prius-640x360.mp4
        format: mp4
        height: '360'
        size: 5225866
        width: '640'
    -   filename: toyota-reasons-prius-640x360.webm
        format: webm
        height: '360'
        size: 4089351
        width: '640'
    -   filename: toyota-reasons-prius-480x270.mp4
        format: mp4
        height: '270'
        size: 3215470
        width: '480'
    title: Toyota Reasons Prius
credits:
-   companyName: Toyota
    companySlug: toyota
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: HSI Productions
    companySlug: hsi-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Gerard de Thame
    personSlug: gerard-de-thame
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Peoples reasons for choosing Hybrid Technology are personal encapsulations.