---
title: Juicy Fruit Ant
subtitle:
slug: juicy-fruit-ant
date: 2006-04-01
role:
headline: directed by Dante Ariola, MJZ
summary: Man mistakenly taunts giant pet ant with Juicy Fruit chewing gum.
excerpt: Man mistakenly taunts giant pet ant with Juicy Fruit chewing gum.
published: false
featured: false
categories:
- Advertising
tags:
- animals
- characters
- animation
- CG
- compositing
- photoreal
albums:
-   cover:
    images:
    -   caption: ''
        filename: juicy-fruit-ant.01.jpg
    -   caption: ''
        filename: juicy-fruit-ant.02.jpg
    -   caption: ''
        filename: juicy-fruit-ant.03.jpg
    -   caption: ''
        filename: juicy-fruit-ant.04.jpg
    -   caption: ''
        filename: juicy-fruit-ant.05.jpg
    -   caption: ''
        filename: juicy-fruit-ant.06.jpg
    -   caption: ''
        filename: juicy-fruit-ant.07.jpg
    path: work/2006/juicy-fruit-ant/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2006/juicy-fruit-ant/videos/juicy-fruit-ant.jpg
    path: work/2006/juicy-fruit-ant/videos/juicy-fruit-ant
    slug: juicy-fruit-ant
    sources:
    -   filename: juicy-fruit-ant-1280x720.mp4
        format: mp4
        height: '720'
        size: 10488994
        width: '1280'
    -   filename: juicy-fruit-ant-960x540.mp4
        format: mp4
        height: '540'
        size: 8154183
        width: '960'
    -   filename: juicy-fruit-ant-640x360.ogv
        format: ogv
        height: '360'
        size: 5092349
        width: '640'
    -   filename: juicy-fruit-ant-640x360.mp4
        format: mp4
        height: '360'
        size: 4939002
        width: '640'
    -   filename: juicy-fruit-ant-640x360.webm
        format: webm
        height: '360'
        size: 3868130
        width: '640'
    -   filename: juicy-fruit-ant-480x270.mp4
        format: mp4
        height: '270'
        size: 3211267
        width: '480'
    title: Juicy Fruit Ant
credits:
-   companyName: Juicy Fruit
    companySlug: juicy-fruit
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Wieden + Kennedy
    companySlug: wk
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Dante Ariola
    personSlug: dante-ariol
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Mitch Drain
    personSlug: mitch-drain
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
-   personName: Denis Gauthier
    personSlug: denis-gauthier
    roleSlug: cg-lighting
    roleTitle: CG Lighting
    visible: false
---
Man mistakenly taunts giant pet ant with Juicy Fruit chewing gum.