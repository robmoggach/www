---
title: Mercedes | "First Is Forever"
subtitle: 'First Is Forever'
slug: mercedes-first-is-forever
date: 2018-03-01
role:
    - Director
    - VFX Supervisor
    - Creative Director
    - Animator
    - TD
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: mercedes-first-is-forever.01.jpg
          - caption: ''
            filename: mercedes-first-is-forever.02.jpg
          - caption: ''
            filename: mercedes-first-is-forever.03.jpg
          - caption: ''
            filename: mercedes-first-is-forever.04.jpg
          - caption: ''
            filename: mercedes-first-is-forever.05.jpg
          - caption: ''
            filename: mercedes-first-is-forever.06.jpg
          - caption: ''
            filename: mercedes-first-is-forever.07.jpg
      path: work/2018/mercedes-first-is-forever/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2018/mercedes-first-is-forever/videos/mercedes-first-is-forever/mercedes-first-is-forever.jpg
      path: work/2018/mercedes-first-is-forever/videos/mercedes-first-is-forever
      slug: mercedes-first-is-forever
      provider: html5
      sources:
          - filename: mercedes-first-is-forever-1280x720.mp4
            format: mp4
            height: '720'
            size:
            width: '1280'
          - filename: mercedes-first-is-forever-960x540.mp4
            format: mp4
            height: '540'
            size:
            width: '960'
          - filename: mercedes-first-is-forever-480x270.ogv
            format: ogv
            height: '270'
            size:
            width: '480'
          - filename: mercedes-first-is-forever-640x360.mp4
            format: mp4
            height: '360'
            size:
            width: '640'
          - filename: mercedes-first-is-forever-640x360.webm
            format: webm
            height: '360'
            size:
            width: '640'
          - filename: mercedes-first-is-forever-480x270.mp4
            format: mp4
            height: '270'
            size:
            width: '480'
      title: Mercedes "First Is Forever"
credits:
    - companyName: The Artery
      companySlug: the-artery
      roleSlug: production
      roleTitle: Production
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Deborah Sullivan
      personSlug: deborah-sullivan
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Paul Cameron, ASC
      personSlug: paul-cameron
      roleSlug: director
      roleTitle: Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: director
      roleTitle: Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Vico Sharibani
      personSlug: vico-sharibani
      roleSlug: director
      roleTitle: Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Vico Sharibani
      personSlug: vico-sharibani
      roleSlug: Creative Director
      roleTitle: Creative Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Lynzi Grant
      personSlug: lynzi-grant
      roleSlug: vfx-producer
      roleTitle: Producer
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
---

Vico Sharibani from The Artery in NYC approached me with the opportunity to co-direct two fully CGI spots with friend and past collaborator, Paul Cameron. As well as direct the spots, I was tasked with VFX Supervising what would ultimately be a revolutionary, non-traditional process.

Early in our pre-production it was clear that location independence was going to be essential. Paul was in LA, I was in Toronto and Vico and our production team were in NYC. I built a satellite studio in Toronto with local crew I trusted and started designing, building and concepting. As we began our previs remotely we ran into challenges with the time it took for Paul to be happy with the camera work. Needing to get a camera in his hand, I approached my good friend Scott Metzger from [Nurulize](http://nurulize.com/) to see if his software had progressed to the point where we could do remote VR sessions.
