---
title: Alfa Romeo Restart Dreaming
subtitle:
slug: alfa-romeo-restart-dreaming
date: 2003-07-01
role:
headline: directed by Tony Kaye, BRW Italia
summary: An abstract dream follows the new Alfa Romeo along it's journey.
excerpt: An abstract dream follows the new Alfa Romeo along it's journey.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: alfa-romeo-restart-dreaming.01.jpg
    -   caption: ''
        filename: alfa-romeo-restart-dreaming.02.jpg
    -   caption: ''
        filename: alfa-romeo-restart-dreaming.03.jpg
    -   caption: ''
        filename: alfa-romeo-restart-dreaming.04.jpg
    -   caption: ''
        filename: alfa-romeo-restart-dreaming.05.jpg
    path: work/2003/alfa-romeo-restart-dreaming/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/alfa-romeo-restart-dreaming/videos/alfa-romeo-restart-dreaming.jpg
    path: work/2003/alfa-romeo-restart-dreaming/videos/alfa-romeo-restart-dreaming
    slug: alfa-romeo-restart-dreaming
    sources:
    -   filename: alfa-romeo-restart-dreaming.960x720.mp4
        format: mp4
        height: '720'
        size: 5596636
        width: '960'
    -   filename: alfa-romeo-restart-dreaming.1280x960.mp4
        format: mp4
        height: '960'
        size: 5350334
        width: '1280'
    -   filename: alfa-romeo-restart-dreaming.640x360.ogv
        format: ogv
        height: '360'
        size: 2594168
        width: '640'
    -   filename: alfa-romeo-restart-dreaming.640x360.webm
        format: webm
        height: '360'
        size: 2575482
        width: '640'
    -   filename: alfa-romeo-restart-dreaming.640x480.mp4
        format: mp4
        height: '480'
        size: 2536320
        width: '640'
    -   filename: alfa-romeo-restart-dreaming.480x360.mp4
        format: mp4
        height: '360'
        size: 2419915
        width: '480'
    title: Alfa Romeo Restart Dreaming
-   caption: ''
    cover: work/2003/alfa-romeo-restart-dreaming/videos/alfa-romeo-restart-dreaming-hd.jpg
    path: work/2003/alfa-romeo-restart-dreaming/videos/alfa-romeo-restart-dreaming-hd
    slug: alfa-romeo-restart-dreaming-hd
    sources:
    -   filename: alfa-romeo-restart-dreaming-960x540.mp4
        format: mp4
        height: '540'
        size: 5596636
        width: '960'
    -   filename: alfa-romeo-restart-dreaming-1280x720.mp4
        format: mp4
        height: '720'
        size: 5350334
        width: '1280'
    -   filename: alfa-romeo-restart-dreaming-640x360.ogv
        format: ogv
        height: '360'
        size: 2594168
        width: '640'
    -   filename: alfa-romeo-restart-dreaming-640x360.webm
        format: webm
        height: '360'
        size: 2575482
        width: '640'
    -   filename: alfa-romeo-restart-dreaming-640x360.mp4
        format: mp4
        height: '360'
        size: 2536320
        width: '640'
    -   filename: alfa-romeo-restart-dreaming-480x270.mp4
        format: mp4
        height: '270'
        size: 2419915
        width: '480'
    title: Alfa Romeo Restart Dreaming Hd
credits: []
---
Shot on different locations in north eastern Spain, the challenge of this
commercial was to create a beautiful dream environment. Painterly, abstract
imagery and epic skies were a theme throughout which served to detach us from
the reality of the environment. Under the strain of a tight schedule and
limiting locations, Director Tony Kaye managed to capture images which
effortlessly captured this mood. As the car travels through the dream it
encounters various surreal obstacles. Giraffe-women, mermaids, metal fish, and
a black monolith were some of the CG elements created. Beautiful captivating
painted faces in the clouds and under water further enhance the dream.
Finished in less than a week of shooting and less than a week of post this
commercial was an endurance test as well as a creative experiment. The
commercial was driven by Italian production company BRW, line produced by
Spanish production company The Lift, CG was produced from Santa Monica, CA by
CG artists in Korea, and I supervised and finished the job at The Mill in
London. Coordinating all these different creatives and resources was a task
within itself.