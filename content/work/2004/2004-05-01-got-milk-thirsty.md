---
title: Got Milk Thirsty
subtitle:
slug: got-milk-thirsty
date: 2004-05-01
role:
headline: directed by Rupert Sanders, MJZ
summary: Skeleton drinks milk, turns into husband.
excerpt: Skeleton drinks milk, turns into husband.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: got-milk-thirsty.01.jpg
    -   caption: ''
        filename: got-milk-thirsty.02.jpg
    -   caption: ''
        filename: got-milk-thirsty.03.jpg
    -   caption: ''
        filename: got-milk-thirsty.04.jpg
    path: work/2004/got-milk-thirsty/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/got-milk-thirsty/videos/got-milk-thirsty.jpg
    path: work/2004/got-milk-thirsty/videos/got-milk-thirsty
    slug: got-milk-thirsty
    sources:
    -   filename: got-milk-thirsty-1280x720.mp4
        format: mp4
        height: '720'
        size: 6422495
        width: '1280'
    -   filename: got-milk-thirsty-960x540.mp4
        format: mp4
        height: '540'
        size: 3936610
        width: '960'
    -   filename: got-milk-thirsty-640x360.mp4
        format: mp4
        height: '360'
        size: 2524588
        width: '640'
    -   filename: got-milk-thirsty-640x360.ogv
        format: ogv
        height: '360'
        size: 2153525
        width: '640'
    -   filename: got-milk-thirsty-480x270.mp4
        format: mp4
        height: '270'
        size: 1531591
        width: '480'
    -   filename: got-milk-thirsty-640x360.webm
        format: webm
        height: '360'
        size: 1497080
        width: '640'
    title: Got Milk Thirsty
credits:
-   companyName: Milk
    companySlug: milk
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Rupert Sanders
    personSlug: rupert-sanders
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Skeleton drinks milk, turns into husband.