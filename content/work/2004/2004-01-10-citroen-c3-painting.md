---
title: Citroen C3 Painting
subtitle:
slug: citroen-c3-painting
date: 2004-01-10
role:
headline: directed by Tarsem, @radical media
summary: Art thief is outwitted by a painting with a mind of it's own.
excerpt: Art thief is outwitted by a painting with a mind of it's own.
published: false
featured: false
categories: []
tags:
- compositing
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: citroen-c3-painting.01.jpg
    -   caption: ''
        filename: citroen-c3-painting.02.jpg
    -   caption: ''
        filename: citroen-c3-painting.03.jpg
    -   caption: ''
        filename: citroen-c3-painting.04.jpg
    -   caption: ''
        filename: citroen-c3-painting.05.jpg
    path: work/2004/citroen-c3-painting/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/citroen-c3-painting/videos/citroen-c3-painting.jpg
    path: work/2004/citroen-c3-painting/videos/citroen-c3-painting
    slug: citroen-c3-painting
    sources:
    -   filename: citroen-c3-painting.1280x960.mp4
        format: mp4
        height: '960'
        size: 8225229
        width: '1280'
    -   filename: citroen-c3-painting.960x720.mp4
        format: mp4
        height: '720'
        size: 4719091
        width: '960'
    -   filename: citroen-c3-painting.640x480.mp4
        format: mp4
        height: '480'
        size: 3467900
        width: '640'
    -   filename: citroen-c3-painting.640x360.ogv
        format: ogv
        height: '360'
        size: 3266606
        width: '640'
    -   filename: citroen-c3-painting.480x360.mp4
        format: mp4
        height: '360'
        size: 2134686
        width: '480'
    -   filename: citroen-c3-painting.640x360.webm
        format: webm
        height: '360'
        size: 2050310
        width: '640'
    title: Citroen C3 Painting
credits:
-   companyName: Citroen
    companySlug: citroen
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: '@Radical Media'
    companySlug: radical-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'Tarsem '
    personSlug: tarsem
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
