---
title: New Era
subtitle: NBA
slug: new-era-nba
date: 2017-12-16
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
    - Advertising
tags: []
albums:
    - cover:
      images: []
      path: work/2017/new-era-nba/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: ''
      path: work/2017/new-era-nba/videos/new-era-nba
      slug: new-era-nba
      provider: html5
      sources:
          - filename: new_era-nba-960x540.mp4
            format: mp4
            height: '540'
            size: 3702910
            width: '960'
          - filename: new_era-nba-1280x720.mp4
            format: mp4
            height: '720'
            size: 3702041
            width: '1280'
          - filename: new_era-nba-640x360.mp4
            format: mp4
            height: '360'
            size: 3695696
            width: '640'
          - filename: new_era-nba-480x270.mp4
            format: mp4
            height: '270'
            size: 3693031
            width: '480'
          - filename: new_era-nba-640x360.ogv
            format: ogv
            height: '360'
            size: 2899084
            width: '640'
          - filename: new_era-nba-640x360.webm
            format: webm
            height: '360'
            size: 1202369
            width: '640'
      title: New Era Nba
credits: []
---

This was a simple yet elegant combination of multiple elements shot in camera.
The various elements were first cleaned of any imperfections and then combined
using a variety of mixes and layering.
