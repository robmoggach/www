---
title: FDA Anti-Smoking "7000 Chemicals"
subtitle: 
slug: fda-anti-smoking-7000-chemicals
date: 2014-09-01
role: 
headline: ''
summary: 
excerpt: 
published: true
featured: true
categories:
- Advertising
tags: []
albums:
- cover: 
  images:
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.01.jpg
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.02.jpg
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.03.jpg
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.04.jpg
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.05.jpg
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.06.jpg
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.07.jpg
  - caption: ''
    filename: fda-anti-smoking-7000-chemicals.08.jpg
  path: work/2014/fda-anti-smoking-7000-chemicals/images/stills
  slug: stills
  title: Stills
videos:
- caption: ''
  cover: work/2014/fda-anti-smoking-7000-chemicals/videos/fda-anti-smoking-7000-chemicals.jpg
  path: work/2014/fda-anti-smoking-7000-chemicals/videos/fda-anti-smoking-7000-chemicals
  slug: fda-anti-smoking-7000-chemicals
  sources:
  - filename: fda-antismoking-7000-chemicals-1280x720.mp4
    format: mp4
    height: 720
    size: 12084304
    width: 1280
  - filename: fda-antismoking-7000-chemicals-960x540.mp4
    format: mp4
    height: 540
    size: 7993566
    width: 960
  - filename: fda-antismoking-7000-chemicals-640x360.mp4
    format: mp4
    height: 360
    size: 5140309
    width: 640
  - filename: fda-antismoking-7000-chemicals-640x360.webm
    format: webm
    height: 360
    size: 3554134
    width: 640
  - filename: fda-antismoking-7000-chemicals-480x270.ogv
    format: ogv
    height: 270
    size: 3430342
    width: 480
  - filename: fda-antismoking-7000-chemicals-480x270.mp4
    format: mp4
    height: 270
    size: 3387398
    width: 480
  title: Fda Anti Smoking 7000 Chemicals
credits: []

---
