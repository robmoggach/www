---
title: FDA Anti-Smoking
subtitle: Band
slug: fda-antismoking-band
date: 2014-12-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: fda-antismoking-band.01.jpg
          - caption: ''
            filename: fda-antismoking-band.02.jpg
          - caption: ''
            filename: fda-antismoking-band.03.jpg
          - caption: ''
            filename: fda-antismoking-band.04.jpg
          - caption: ''
            filename: fda-antismoking-band.05.jpg
      path: work/2014/fda-antismoking-band/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2014/fda-antismoking-band/videos/fda-anti-smoking-band.jpg
      path: work/2014/fda-antismoking-band/videos/fda-anti-smoking-band
      slug: fda-anti-smoking-band
      provider: html5
      sources:
          - filename: fda-anti-smoking-band-1280x720.mp4
            format: mp4
            height: '720'
            size: 6268140
            width: '1280'
          - filename: fda-anti-smoking-band-960x540.mp4
            format: mp4
            height: '540'
            size: 4948279
            width: '960'
          - filename: fda-anti-smoking-band-640x360.ogv
            format: ogv
            height: '360'
            size: 3416289
            width: '640'
          - filename: fda-anti-smoking-band-640x360.mp4
            format: mp4
            height: '360'
            size: 3320868
            width: '640'
          - filename: fda-anti-smoking-band-640x360.webm
            format: webm
            height: '360'
            size: 3266673
            width: '640'
          - filename: fda-anti-smoking-band-480x270.mp4
            format: mp4
            height: '270'
            size: 2314627
            width: '480'
      title: Fda Anti Smoking Band
credits: []
---
