---
title: Coca-Cola | "Rocket Launch"
subtitle: 'Rocket Launch'
slug: coke-rocket-launch
date: 2014-12-22
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: coke-rocket-launch.01.jpg
          - caption: ''
            filename: coke-rocket-launch.02.jpg
          - caption: ''
            filename: coke-rocket-launch.03.jpg
          - caption: ''
            filename: coke-rocket-launch.04.jpg
          - caption: ''
            filename: coke-rocket-launch.05.jpg
          - caption: ''
            filename: coke-rocket-launch.06.jpg
          - caption: ''
            filename: coke-rocket-launch.07.jpg
      path: work/2014/coke-rocket-launch/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2014/coke-rocket-launch/videos/coke-rocket-launch.jpg
      path: work/2014/coke-rocket-launch/videos/coke-rocket-launch
      provider: html5
      slug: coke-rocket-launch
      sources:
          - filename: coke-rocket-launch-1280x720.mp4
            format: mp4
            height: '720'
            size: 8324444
            width: '1280'
          - filename: coke-rocket-launch-960x540.mp4
            format: mp4
            height: '540'
            size: 5707207
            width: '960'
          - filename: coke-rocket-launch-640x360.ogv
            format: ogv
            height: '360'
            size: 4187243
            width: '640'
          - filename: coke-rocket-launch-640x360.webm
            format: webm
            height: '360'
            size: 3951007
            width: '640'
          - filename: coke-rocket-launch-640x360.mp4
            format: mp4
            height: '360'
            size: 3883704
            width: '640'
          - filename: coke-rocket-launch-480x270.mp4
            format: mp4
            height: '270'
            size: 2639336
            width: '480'
      title: Coke Rocket Launch
credits: []
---
