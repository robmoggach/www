---
title: Nexcare | "Dumpster"
subtitle: 'Dumpster'
slug: nexcare-dumpster
date: 2014-01-01
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: nexcare-dumpster.01.jpg
          - caption: ''
            filename: nexcare-dumpster.02.jpg
          - caption: ''
            filename: nexcare-dumpster.03.jpg
          - caption: ''
            filename: nexcare-dumpster.04.jpg
          - caption: ''
            filename: nexcare-dumpster.05.jpg
          - caption: ''
            filename: nexcare-dumpster.06.jpg
      path: work/2014/nexcare-dumpster/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2014/nexcare-dumpster/videos/nexcare-dumpster.jpg
      path: work/2014/nexcare-dumpster/videos/nexcare-dumpster
      slug: nexcare-dumpster
      sources:
          - filename: nexcare-dumpster-dirTitled-1280x720.mp4
            format: mp4
            height: '720'
            size: 13696186
            width: '1280'
          - filename: nexcare-dumpster-dirTitled-960x540.mp4
            format: mp4
            height: '540'
            size: 10398268
            width: '960'
          - filename: nexcare-dumpster-dirTitled-640x360.webm
            format: webm
            height: '360'
            size: 7768387
            width: '640'
          - filename: nexcare-dumpster-dirTitled-640x360.ogv
            format: ogv
            height: '360'
            size: 7700693
            width: '640'
          - filename: nexcare-dumpster-dirTitled-640x360.mp4
            format: mp4
            height: '360'
            size: 6514524
            width: '640'
          - filename: nexcare-dumpster-dirTitled-480x270.mp4
            format: mp4
            height: '270'
            size: 4257041
            width: '480'
      title: Nexcare Dumpster
credits: []
---
