---
title: BMW
subtitle: 'Change'
slug: bmw-change-i8-launch
date: 2014-12-05
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags:
    - compositing
    - automotive
    - set extensions
albums:
    - cover:
      images:
          - caption: ''
            filename: bmw-change-i8-launch.01.jpg
          - caption: ''
            filename: bmw-change-i8-launch.02.jpg
          - caption: ''
            filename: bmw-change-i8-launch.03.jpg
          - caption: ''
            filename: bmw-change-i8-launch.04.jpg
          - caption: ''
            filename: bmw-change-i8-launch.05.jpg
          - caption: ''
            filename: bmw-change-i8-launch.06.jpg
          - caption: ''
            filename: bmw-change-i8-launch.07.jpg
      path: work/2014/bmw-change-i8-launch/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2014/bmw-change-i8-launch/videos/bmw-change-i8-launch.jpg
      path: work/2014/bmw-change-i8-launch/videos/bmw-change-i8-launch
      slug: bmw-change-i8-launch
      provider: html5
      sources:
          - filename: bmw-change-i8-launch-1280x720.mp4
            format: mp4
            height: '720'
            size: 4668679
            width: '1280'
      title: Bmw Change I8 Launch
credits:
    - companyName: Pulse Films
      companySlug: pulse-films
      roleSlug: production
      roleTitle: Production Company
      visible: true
    - companyName: Pulse Films
      companySlug: pulse-films
      personName: Michael Haussman
      personSlug: michael-haussman
      roleSlug: director
      roleTitle: Director
      visible: true
    - personName: Salvatore Totino
      personSlug: salvatore-totino
      roleSlug: dp
      roleTitle: Director of Photography
      visible: true
    - companyName: MassMarket
      companySlug: massmarket
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: MassMarket
      companySlug: massmarket
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
---
