---
title: FDA Anti-Smoking
subtitle: Contract
slug: fda-anti-smoking-contract
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
    - cover:
      images: []
      path: work/2014/fda-anti-smoking-contract/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: ''
      path: work/2014/fda-anti-smoking-contract/videos/fda-anti-smoking-contract
      slug: fda-anti-smoking-contract
      provider: html5
      sources:
          - filename: fda-anti-smoking-contract-1280x720.mp4
            format: mp4
            height: '720'
            size: 6946310
            width: '1280'
      title: Fda Anti Smoking Contract
credits: []
---
