---
title: Zach Gold "Killer Heels"
subtitle:
slug: zach-gold-killer-heels
date: 2014-08-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
- Installation
tags:
- VFX
- effects
- compositing
- colour
- set extensions
- surreal
albums:
-   cover:
    images:
    -   caption: ''
        filename: zach-gold-killer-heels.01.jpg
    -   caption: ''
        filename: zach-gold-killer-heels.02.jpg
    -   caption: ''
        filename: zach-gold-killer-heels.03.jpg
    -   caption: ''
        filename: zach-gold-killer-heels.04.jpg
    -   caption: ''
        filename: zach-gold-killer-heels.05.jpg
    -   caption: ''
        filename: zach-gold-killer-heels.06.jpg
    path: work/2014/zach-gold-killer-heels/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2014/zach-gold-killer-heels/videos/zach-gold-killer-heels.jpg
    path: work/2014/zach-gold-killer-heels/videos/zach-gold-killer-heels
    slug: zach-gold-killer-heels
    sources:
    -   filename: zach-gold-killer-heels-1280x720.mp4
        format: mp4
        height: '720'
        size: 38682337
        width: '1280'
    -   filename: zach-gold-killer-heels-960x540.mp4
        format: mp4
        height: '540'
        size: 38603251
        width: '960'
    -   filename: zach-gold-killer-heels-480x270.mp4
        format: mp4
        height: '270'
        size: 38581576
        width: '480'
    -   filename: zach-gold-killer-heels-640x360.mp4
        format: mp4
        height: '360'
        size: 38557034
        width: '640'
    -   filename: zach-gold-killer-heels-640x360.ogv
        format: ogv
        height: '360'
        size: 34871009
        width: '640'
    -   filename: zach-gold-killer-heels-640x360.webm
        format: webm
        height: '360'
        size: 11314988
        width: '640'
    title: Zach Gold Killer Heels
credits:
-   personName: Zach Gold
    personSlug: zach-gold
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: MassMarket
    companySlug: massmarket
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: creative-director
    roleTitle: Creative Director
    visible: true
-   companyName: MassMarket
    companySlug: massmarket
    personName: Louisa Cartwright
    personSlug: louisa-cartwright
    roleSlug: vfx-ep
    roleTitle: Executive Producer
    visible: true
-   companyName: MassMarket
    companySlug: massmarket
    personName: Peter Nelson
    personSlug: peter-nelson
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: true
-   companyName: MassMarket
    companySlug: massmarket
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: colorist
    roleTitle: Colorist
    visible: true
-   companyName: MassMarket
    companySlug: massmarket
    personName: Julien Brami
    personSlug: julien-brami
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
-   companyName: MassMarket
    companySlug: massmarket
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
---
