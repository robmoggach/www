---
title: Acura Rsx Roads
subtitle:
slug: acura-rsx-roads
date: 2005-03-01
role:
headline: directed by Mark Romanek, Anonymous Content
summary:
excerpt:
published: false
featured: false
categories: []
tags:
- CG
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: acura-rsx-roads.01.jpg
    -   caption: ''
        filename: acura-rsx-roads.02.jpg
    -   caption: ''
        filename: acura-rsx-roads.03.jpg
    -   caption: ''
        filename: acura-rsx-roads.04.jpg
    -   caption: ''
        filename: acura-rsx-roads.05.jpg
    path: work/2005/acura-rsx-roads/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/acura-rsx-roads/videos/acura-rsx-roads.jpg
    path: work/2005/acura-rsx-roads/videos/acura-rsx-roads
    slug: acura-rsx-roads
    sources:
    -   filename: acura-rsx-roads-1280x720.mp4
        format: mp4
        height: '720'
        size: 10124274
        width: '1280'
    -   filename: acura-rsx-roads-960x540.mp4
        format: mp4
        height: '540'
        size: 5924166
        width: '960'
    -   filename: acura-rsx-roads-640x360.mp4
        format: mp4
        height: '360'
        size: 3820188
        width: '640'
    -   filename: acura-rsx-roads-640x360.ogv
        format: ogv
        height: '360'
        size: 3280018
        width: '640'
    -   filename: acura-rsx-roads-480x270.mp4
        format: mp4
        height: '270'
        size: 2425870
        width: '480'
    -   filename: acura-rsx-roads-640x360.webm
        format: webm
        height: '360'
        size: 2364315
        width: '640'
    title: Acura Rsx Roads
credits:
-   companyName: Acura
    companySlug: acura
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Anonymous Content
    companySlug: anonymous-content
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Mark Romanek
    personSlug: mark-romanek
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Digital Domain
    companySlug: digital-domain
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
-   companyName: Digital Domain
    companySlug: digital-domain
    personName: Brad Parker
    personSlug: brad-parker
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
