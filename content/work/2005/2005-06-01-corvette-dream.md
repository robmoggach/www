---
title: Corvette Dream
subtitle:
slug: corvette-dream
date: 2005-06-01
role:
headline: directed by Guy Ritchie, Anonymous Content
summary: Boy drives corvette. Not legal.
excerpt: Boy drives corvette. Not legal.
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- VFX
- photoreal
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: corvette-dream.01.jpg
    -   caption: ''
        filename: corvette-dream.02.jpg
    -   caption: ''
        filename: corvette-dream.03.jpg
    -   caption: ''
        filename: corvette-dream.04.jpg
    path: work/2005/corvette-dream/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/corvette-dream/videos/corvette-dream.jpg
    path: work/2005/corvette-dream/videos/corvette-dream
    slug: corvette-dream
    sources:
    -   filename: corvette-dream-1280x720.mp4
        format: mp4
        height: '720'
        size: 21120514
        width: '1280'
    -   filename: corvette-dream-960x540.mp4
        format: mp4
        height: '540'
        size: 14165192
        width: '960'
    -   filename: corvette-dream-640x360.ogv
        format: ogv
        height: '360'
        size: 9624710
        width: '640'
    -   filename: corvette-dream-640x360.mp4
        format: mp4
        height: '360'
        size: 9099189
        width: '640'
    -   filename: corvette-dream-640x360.webm
        format: webm
        height: '360'
        size: 7012191
        width: '640'
    -   filename: corvette-dream-480x270.mp4
        format: mp4
        height: '270'
        size: 5832203
        width: '480'
    title: Corvette Dream
credits:
-   companyName: Anonymous Content
    companySlug: anonymous-content
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Guy Ritchie
    personSlug: guy-ritchie
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Boy drives corvette. Not legal.