---
title: Nintendo Road Trip
subtitle:
slug: nintendo-road-trip
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: nintendo-road-trip.01.jpg
    -   caption: ''
        filename: nintendo-road-trip.02.jpg
    -   caption: ''
        filename: nintendo-road-trip.03.jpg
    -   caption: ''
        filename: nintendo-road-trip.04.jpg
    path: work/2005/nintendo-road-trip/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/nintendo-road-trip/videos/nintendo-road-trip.jpg
    path: work/2005/nintendo-road-trip/videos/nintendo-road-trip
    slug: nintendo-road-trip
    sources:
    -   filename: nintendo-road-trip-1280x720.mp4
        format: mp4
        height: '720'
        size: 10526228
        width: '1280'
    -   filename: nintendo-road-trip-960x540.mp4
        format: mp4
        height: '540'
        size: 6194748
        width: '960'
    -   filename: nintendo-road-trip-640x360.ogv
        format: ogv
        height: '360'
        size: 4691823
        width: '640'
    -   filename: nintendo-road-trip-640x360.mp4
        format: mp4
        height: '360'
        size: 4484299
        width: '640'
    -   filename: nintendo-road-trip-640x360.webm
        format: webm
        height: '360'
        size: 3762847
        width: '640'
    -   filename: nintendo-road-trip-480x270.mp4
        format: mp4
        height: '270'
        size: 3053197
        width: '480'
    title: Nintendo Road Trip
credits: []
---
