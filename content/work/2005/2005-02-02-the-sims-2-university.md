---
title: The Sims 2 University
subtitle:
slug: the-sims-2-university
date: 2005-02-02
role:
headline: directed by ACNE
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: the-sims-2-university.01.jpg
    -   caption: ''
        filename: the-sims-2-university.02.jpg
    -   caption: ''
        filename: the-sims-2-university.03.jpg
    -   caption: ''
        filename: the-sims-2-university.04.jpg
    path: work/2005/the-sims-2-university/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/the-sims-2-university/videos/the-sims-2-university.jpg
    path: work/2005/the-sims-2-university/videos/the-sims-2-university
    slug: the-sims-2-university
    sources:
    -   filename: the-sims-2-university.1280x960.mp4
        format: mp4
        height: '960'
        size: 10433399
        width: '1280'
    -   filename: the-sims-2-university.960x720.mp4
        format: mp4
        height: '720'
        size: 6184423
        width: '960'
    -   filename: the-sims-2-university.640x360.ogv
        format: ogv
        height: '360'
        size: 5042873
        width: '640'
    -   filename: the-sims-2-university.640x480.mp4
        format: mp4
        height: '480'
        size: 4571666
        width: '640'
    -   filename: the-sims-2-university.640x360.webm
        format: webm
        height: '360'
        size: 3347442
        width: '640'
    -   filename: the-sims-2-university.480x360.mp4
        format: mp4
        height: '360'
        size: 2982715
        width: '480'
    title: The Sims 2 University
credits:
-   companyName: ACNE Collective
    companySlug: acne-collective
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
