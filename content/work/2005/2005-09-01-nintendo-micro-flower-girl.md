---
title: Nintendo Micro Flower Girl
subtitle:
slug: nintendo-micro-flower-girl
date: 2005-09-01
role:
headline: directed by Nathan McGuinness, Kommitted
summary: Flowers grow behind a girl as she walks through her world.
excerpt: Flowers grow behind a girl as she walks through her world.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- set extensions
- projection
- environment
- tracking
albums:
-   cover:
    images:
    -   caption: ''
        filename: nintendo-micro-flower-girl.01.jpg
    -   caption: ''
        filename: nintendo-micro-flower-girl.02.jpg
    -   caption: ''
        filename: nintendo-micro-flower-girl.03.jpg
    -   caption: ''
        filename: nintendo-micro-flower-girl.04.jpg
    -   caption: ''
        filename: nintendo-micro-flower-girl.05.jpg
    path: work/2005/nintendo-micro-flower-girl/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/nintendo-micro-flower-girl/videos/nintendo-micro-flower-girl.jpg
    path: work/2005/nintendo-micro-flower-girl/videos/nintendo-micro-flower-girl
    slug: nintendo-micro-flower-girl
    sources:
    -   filename: nintendo-micro-flower-girl-1280x720.mp4
        format: mp4
        height: '720'
        size: 6543752
        width: '1280'
    -   filename: nintendo-micro-flower-girl-960x540.mp4
        format: mp4
        height: '540'
        size: 3241169
        width: '960'
    -   filename: nintendo-micro-flower-girl-640x360.ogv
        format: ogv
        height: '360'
        size: 2388322
        width: '640'
    -   filename: nintendo-micro-flower-girl-640x360.mp4
        format: mp4
        height: '360'
        size: 2353707
        width: '640'
    -   filename: nintendo-micro-flower-girl-640x360.webm
        format: webm
        height: '360'
        size: 1661757
        width: '640'
    -   filename: nintendo-micro-flower-girl-480x270.mp4
        format: mp4
        height: '270'
        size: 1541682
        width: '480'
    title: Nintendo Micro Flower Girl
credits:
-   companyName: Nintendo
    companySlug: nintendo
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Kommitted Films
    companySlug: kommitted-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Nathan McGuinness
    personSlug: nathan-mcguinness
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Flowers grow behind a girl as she walks through her world.