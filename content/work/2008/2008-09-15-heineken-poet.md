---
title: Heineken Poet
subtitle:
slug: heineken-poet
date: 2008-09-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags:
- colour
- compositing
- Live Action
albums:
-   cover:
    images:
    -   caption: ''
        filename: heineken-poet.01.jpg
    -   caption: ''
        filename: heineken-poet.02.jpg
    -   caption: ''
        filename: heineken-poet.03.jpg
    -   caption: ''
        filename: heineken-poet.04.jpg
    -   caption: ''
        filename: heineken-poet.05.jpg
    -   caption: ''
        filename: heineken-poet.06.jpg
    -   caption: ''
        filename: heineken-poet.07.jpg
    -   caption: ''
        filename: heineken-poet.08.jpg
    -   caption: ''
        filename: heineken-poet.09.jpg
    -   caption: ''
        filename: heineken-poet.10.jpg
    path: work/2008/heineken-poet/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2008/heineken-poet/videos/heineken-poet-30.jpg
    path: work/2008/heineken-poet/videos/heineken-poet-30
    slug: heineken-poet-30
    sources:
    -   filename: heineken-poet-30-1280x720.mp4
        format: mp4
        height: '720'
        size: 9681327
        width: '1280'
    -   filename: heineken-poet-30-960x540.mp4
        format: mp4
        height: '540'
        size: 7022305
        width: '960'
    -   filename: heineken-poet-30-640x360.mp4
        format: mp4
        height: '360'
        size: 3568300
        width: '640'
    -   filename: heineken-poet-30-640x360.ogv
        format: ogv
        height: '360'
        size: 2977537
        width: '640'
    -   filename: heineken-poet-30-640x360.webm
        format: webm
        height: '360'
        size: 2480285
        width: '640'
    -   filename: heineken-poet-30-480x270.mp4
        format: mp4
        height: '270'
        size: 2185210
        width: '480'
    title: Heineken Poet 30
-   caption: ''
    cover: work/2008/heineken-poet/videos/heineken-poet-45.jpg
    path: work/2008/heineken-poet/videos/heineken-poet-45
    slug: heineken-poet-45
    sources:
    -   filename: heineken-poet-45-1280x720.mp4
        format: mp4
        height: '720'
        size: 14942914
        width: '1280'
    -   filename: heineken-poet-45-960x540.mp4
        format: mp4
        height: '540'
        size: 10786405
        width: '960'
    -   filename: heineken-poet-45-640x360.mp4
        format: mp4
        height: '360'
        size: 5347655
        width: '640'
    -   filename: heineken-poet-45-640x360.ogv
        format: ogv
        height: '360'
        size: 4418960
        width: '640'
    -   filename: heineken-poet-45-640x360.webm
        format: webm
        height: '360'
        size: 3717578
        width: '640'
    -   filename: heineken-poet-45-480x270.mp4
        format: mp4
        height: '270'
        size: 3226882
        width: '480'
    title: Heineken Poet 45
-   caption: ''
    cover: work/2008/heineken-poet/videos/heineken-poet-60.jpg
    path: work/2008/heineken-poet/videos/heineken-poet-60
    slug: heineken-poet-60
    sources:
    -   filename: heineken-poet-60-1280x720.mp4
        format: mp4
        height: '720'
        size: 19441247
        width: '1280'
    -   filename: heineken-poet-60-960x540.mp4
        format: mp4
        height: '540'
        size: 14060607
        width: '960'
    -   filename: heineken-poet-60-640x360.mp4
        format: mp4
        height: '360'
        size: 6792068
        width: '640'
    -   filename: heineken-poet-60-640x360.ogv
        format: ogv
        height: '360'
        size: 5881426
        width: '640'
    -   filename: heineken-poet-60-640x360.webm
        format: webm
        height: '360'
        size: 4872908
        width: '640'
    -   filename: heineken-poet-60-480x270.mp4
        format: mp4
        height: '270'
        size: 4099787
        width: '480'
    title: Heineken Poet 60
credits:
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: director
    roleTitle: Director
    visible: true
-   personName: Harris Charalambous
    personSlug: harris-charalambous
    roleSlug: dp
    roleTitle: Director of Photography
    visible: true
-   personName: Nicholas Berglund
    personSlug: nicholas-berglund
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: true
---
