---
title: Careerbuilder Personality
subtitle:
slug: careerbuilder-personality
date: 2008-12-24
role:
headline: directed by Tom Kuntz, MJZ
summary: Man reads ugly resume that comes to life.
excerpt: Man reads ugly resume that comes to life.
published: false
featured: false
categories:
- Advertising
tags:
- animation
- CG
- liquids
- fluids
- photoreal
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: careerbuilder-personality.01.jpg
    -   caption: ''
        filename: careerbuilder-personality.02.jpg
    -   caption: ''
        filename: careerbuilder-personality.03.jpg
    -   caption: ''
        filename: careerbuilder-personality.04.jpg
    -   caption: ''
        filename: careerbuilder-personality.05.jpg
    -   caption: ''
        filename: careerbuilder-personality.06.jpg
    -   caption: ''
        filename: careerbuilder-personality.07.jpg
    -   caption: ''
        filename: careerbuilder-personality.08.jpg
    path: work/2008/careerbuilder-personality/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2008/careerbuilder-personality/videos/careerbuilder-personality.jpg
    path: work/2008/careerbuilder-personality/videos/careerbuilder-personality
    slug: careerbuilder-personality
    sources:
    -   filename: careerbuilder-personality-960x540.mp4
        format: mp4
        height: '540'
        size: 11442297
        width: '960'
    -   filename: careerbuilder-personality-1280x720.mp4
        format: mp4
        height: '720'
        size: 10393982
        width: '1280'
    -   filename: careerbuilder-personality-640x360.mp4
        format: mp4
        height: '360'
        size: 5306481
        width: '640'
    -   filename: careerbuilder-personality-640x360.ogv
        format: ogv
        height: '360'
        size: 5249942
        width: '640'
    -   filename: careerbuilder-personality-640x360.webm
        format: webm
        height: '360'
        size: 5234220
        width: '640'
    -   filename: careerbuilder-personality-480x270.mp4
        format: mp4
        height: '270'
        size: 4684607
        width: '480'
    title: Careerbuilder Personality
credits:
-   companyName: careerbuilder.com
    companySlug: careerbuildercom
    roleSlug: client
    roleTitle: Client
    visible: true
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Tom Kuntz
    personSlug: tom-kuntz
    roleSlug: director
    roleTitle: Director
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
---
Man reads ugly resume that comes to life.