---
title: Uniqlo Heattech
subtitle:
slug: uniqlo-heattech
date: 2010-09-16
role:
headline: directed by Chris Sargent, Park Pictures for Dentsu Japan
summary: The subtle nuances of winter cast a spell on actors Charlize Theron andOrlando
    Bloom in the latest campaign from UNIQLO. Shot in sunny Malibu, CA byChris Sargent,
    the director/DP called upon Dashing Collective and VFXSupervisor Rob Moggach to
    change the Malibu coastline to a a wintry, eastcoast environment and enhance and
    create blowing snow throughout in order toextol the warmth of the Japanese retailer's
    Heattech winter line.
excerpt: The subtle nuances of winter cast a spell on actors Charlize Theron andOrlando
    Bloom in the latest campaign from UNIQLO. Shot in sunny Malibu, CA byChris Sargent,
    the director/DP called upon Dashing Collective and VFXSupervisor Rob Moggach to
    change the Malibu coastline to a a wintry, eastcoast environment and enhance and
    create blowing snow throughout in order toextol the warmth of the Japanese retailer's
    Heattech winter line.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- photoreal
- compositing
- celebrity
- clothing
- colour
albums:
-   cover:
    images:
    -   caption: ''
        filename: uniqlo-heattech-charlize.01.jpg
    -   caption: ''
        filename: uniqlo-heattech-charlize.02.jpg
    -   caption: ''
        filename: uniqlo-heattech-charlize.03.jpg
    -   caption: ''
        filename: uniqlo-heattech-charlize.04.jpg
    path: work/2010/uniqlo-heattech/images/uniqlo-heattech-charlize-stills
    slug: uniqlo-heattech-charlize-stills
    title: Uniqlo Heattech Charlize Stills
-   cover:
    images:
    -   caption: ''
        filename: uniqlo-heattech-orlando.01.jpg
    -   caption: ''
        filename: uniqlo-heattech-orlando.02.jpg
    -   caption: ''
        filename: uniqlo-heattech-orlando.03.jpg
    -   caption: ''
        filename: uniqlo-heattech-orlando.04.jpg
    path: work/2010/uniqlo-heattech/images/uniqlo-heattech-orlando-stills
    slug: uniqlo-heattech-orlando-stills
    title: Uniqlo Heattech Orlando Stills
videos:
-   caption: ''
    cover: work/2010/uniqlo-heattech/videos/uniqlo-heattech-charlize.jpg
    path: work/2010/uniqlo-heattech/videos/uniqlo-heattech-charlize
    slug: uniqlo-heattech-charlize
    sources:
    -   filename: uniqlo-heattech-charlize60.1280x720.mp4
        format: mp4
        height: '720'
        size: 38074566
        width: '1280'
    -   filename: uniqlo-heattech-charlize-960x540.mp4
        format: mp4
        height: '540'
        size: 8845018
        width: '960'
    -   filename: uniqlo-heattech-charlize-640x360.ogv
        format: ogv
        height: '360'
        size: 5778024
        width: '640'
    -   filename: uniqlo-heattech-charlize-640x360.mp4
        format: mp4
        height: '360'
        size: 5580622
        width: '640'
    -   filename: uniqlo-heattech-charlize-640x360.webm
        format: webm
        height: '360'
        size: 4764270
        width: '640'
    -   filename: uniqlo-heattech-charlize-480x270.mp4
        format: mp4
        height: '270'
        size: 3809862
        width: '480'
    title: Uniqlo Heattech Charlize
-   caption: ''
    cover: work/2010/uniqlo-heattech/videos/uniqlo-heattech-orlando.jpg
    path: work/2010/uniqlo-heattech/videos/uniqlo-heattech-orlando
    slug: uniqlo-heattech-orlando
    sources:
    -   filename: uniqlo-heattech-orlando60.1280x720.mp4
        format: mp4
        height: '720'
        size: 38120493
        width: '1280'
    -   filename: uniqlo-heattech-orlando-960x540.mp4
        format: mp4
        height: '540'
        size: 10317936
        width: '960'
    -   filename: uniqlo-heattech-orlando-640x360.ogv
        format: ogv
        height: '360'
        size: 7114254
        width: '640'
    -   filename: uniqlo-heattech-orlando-640x360.mp4
        format: mp4
        height: '360'
        size: 6542304
        width: '640'
    -   filename: uniqlo-heattech-orlando-640x360.webm
        format: webm
        height: '360'
        size: 5884120
        width: '640'
    -   filename: uniqlo-heattech-orlando-480x270.mp4
        format: mp4
        height: '270'
        size: 4589426
        width: '480'
    title: Uniqlo Heattech Orlando
credits:
-   companyName: Uniqlo
    companySlug: uniqlo
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Dentsu Japan
    companySlug: dentsu-japan
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   companyName: Day O Productions
    companySlug: day-o-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Chris Sargent
    personSlug: chris-sargent
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Motoki Tomatsu
    personSlug: motoki-tomatsu
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Meiko Koyama
    personSlug: meiko-koyama
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: Panic & Bob
    companySlug: panic-bob
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Michelle Czukar
    personSlug: michelle-czukar
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Sam McLaren
    personSlug: sam-mclaren
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
---
The subtle nuances of winter cast a spell on actors Charlize Theron and
Orlando Bloom in the latest campaign from UNIQLO. Shot in sunny Malibu, CA by
Chris Sargent, the director/DP called upon Dashing Collective and VFX
Supervisor Rob Moggach to change the Malibu coastline to a a wintry, east
coast environment and enhance and create blowing snow throughout in order to
extol the warmth of the Japanese retailer's Heattech winter line.

Moggach, an accomplished supervisor and artist, spent two weeks removing the
Malibu skyline and Santa Monica mountains and adding a light snowfall for
Theron's beach stroll and surrounding Bloom's cozy Rocky Mountain cabin with
heavier amounts of the white stuff.

"These films were already very cinematic and beautiful so our job was to fall
into the background and enhance already pristine images, says Moggach. "There
was a limited amount of time so all of the snow is animated procedurally on
the Flame as opposed to going the traditional CG route.

This unique project landed in Dashing's studio, courtesy of director Chris
Sargent. Agency Dentsu Japan took a hands off approach with the production,
preferring to see Sargent's vision fully realised. Both the agency and
director, took the same hands-off approach to post-production, trusting
Moggach's craftmanship to enhance the beautifully-filmed spots.

"The agency was fantastically supportive of Chris (Sargent) as a director and
let him take the lead, so he oversaw our work as client, says Dashing EP
Danielle Lyons. "He could've taken the VFX work anywhere and he chose Dashing
knowing Rob's talent and experience was what the film needed. For a
Director/DP with such a strong visual style and extensive VFX knowledge, we
were honoured by the level of trust.

Anchored by Founder/Creative Director Robert Moggach, Executive Producer
Danielle Lyons and a select team of top digital artists, Dashing is tailor-
made for handling projects with demanding and unconventional creative and
technical challenges.