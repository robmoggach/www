---
title: Ea Sports Mma Global Ring
subtitle:
slug: ea-sports-mma-global-ring
date: 2010-12-06
role:
headline: directed by Jason Smith, HSI for Heat SF
summary: Like the Mixed Martial Arts ring, the production world is an internationalarena.
    For the EA Sports MMA spot Global Ring, creative visual effects studioDashing
    Collective was part of a team befitting the Rio de Janeiro-shotcommercial's international
    scope.
excerpt: Like the Mixed Martial Arts ring, the production world is an internationalarena.
    For the EA Sports MMA spot Global Ring, creative visual effects studioDashing
    Collective was part of a team befitting the Rio de Janeiro-shotcommercial's international
    scope.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- photoreal
- CG
- environment
- set extensions
- compositing
- effects
albums:
-   cover:
    images:
    -   caption: ''
        filename: ea-sports-mma-global-ring.01.jpg
    -   caption: ''
        filename: ea-sports-mma-global-ring.02.jpg
    -   caption: ''
        filename: ea-sports-mma-global-ring.03.jpg
    -   caption: ''
        filename: ea-sports-mma-global-ring.04.jpg
    -   caption: ''
        filename: ea-sports-mma-global-ring.05.jpg
    -   caption: ''
        filename: ea-sports-mma-global-ring.06.jpg
    -   caption: ''
        filename: ea-sports-mma-global-ring.07.jpg
    path: work/2010/ea-sports-mma-global-ring/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2010/ea-sports-mma-global-ring/videos/ea-sports-mma-global-ring.jpg
    path: work/2010/ea-sports-mma-global-ring/videos/ea-sports-mma-global-ring
    slug: ea-sports-mma-global-ring
    sources:
    -   filename: ea-sports-mma-global-ring-1280x720.mp4
        format: mp4
        height: '720'
        size: 19262593
        width: '1280'
    -   filename: ea-sports-mma-global-ring-960x540.mp4
        format: mp4
        height: '540'
        size: 12590164
        width: '960'
    -   filename: ea-sports-mma-global-ring-640x360.ogv
        format: ogv
        height: '360'
        size: 6138543
        width: '640'
    -   filename: ea-sports-mma-global-ring-640x360.mp4
        format: mp4
        height: '360'
        size: 6129085
        width: '640'
    -   filename: ea-sports-mma-global-ring-640x360.webm
        format: webm
        height: '360'
        size: 6015421
        width: '640'
    -   filename: ea-sports-mma-global-ring-480x270.mp4
        format: mp4
        height: '270'
        size: 4431914
        width: '480'
    title: Ea Sports Mma Global Ring
-   caption: ''
    cover: work/2010/ea-sports-mma-global-ring/videos/ea-sports-mma-global-ring-60.jpg
    path: work/2010/ea-sports-mma-global-ring/videos/ea-sports-mma-global-ring-60
    slug: ea-sports-mma-global-ring-60
    sources:
    -   filename: ea-sports-mma-global-ring-60-1280x720.mp4
        format: mp4
        height: '720'
        size: 36124444
        width: '1280'
    -   filename: ea-sports-mma-global-ring-60-960x540.mp4
        format: mp4
        height: '540'
        size: 24327408
        width: '960'
    -   filename: ea-sports-mma-global-ring-60-640x360.mp4
        format: mp4
        height: '360'
        size: 11536260
        width: '640'
    -   filename: ea-sports-mma-global-ring-60-640x360.ogv
        format: ogv
        height: '360'
        size: 11522541
        width: '640'
    -   filename: ea-sports-mma-global-ring-60-640x360.webm
        format: webm
        height: '360'
        size: 11432643
        width: '640'
    -   filename: ea-sports-mma-global-ring-60-480x270.mp4
        format: mp4
        height: '270'
        size: 8082098
        width: '480'
    title: Ea Sports Mma Global Ring 60
credits:
-   companyName: EA Sports MMA
    companySlug: ea-sports-mma
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Heat SF
    companySlug: heat-sf
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Steve Sone
    personSlug: steve-sone
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Woody Kurupintsiri
    personSlug: woody-kurupintsiri
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: James Duffy
    personSlug: james-duffy
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   personName: Brian Coates
    personSlug: brian-coates
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: HSI Productions
    companySlug: hsi-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Jason Smith
    personSlug: jason-smith
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Marcelo Durst
    personSlug: marcelo-durst
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
-   personName: Maddi Carlton
    personSlug: maddi-carlton
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Paul Ure
    personSlug: paul-ure
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: Final Cut LA
    companySlug: final-cut-la
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Richard Learoyd
    personSlug: richard-learoyd
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Saima Awan
    personSlug: saima-awan
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Jennifer Miller
    personSlug: jennifer-miller
    roleSlug: edit-prod
    roleTitle: Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Danielle Lyons
    personSlug: danielle-lyons
    roleSlug: vfx-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Debbie Cooke
    personSlug: debbie-cooke
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
---
Like the Mixed Martial Arts ring, the production world is an international
arena. For the EA Sports MMA spot Global Ring, creative visual effects studio
Dashing Collective was part of a team befitting the Rio de Janeiro-shot
commercial's international scope.

Directed by HSI Productions' Jason Smith for San Francisco agency Heat, the
:60 live-action spot gives gamers a gritty, front-row view for sanctioned and
back-alley mixed martial arts fights in exotic locales. Dashing, founded this
year by award-winning VFX supervisor Rob Moggach, has already worked with
several west coast agencies and some of ad land's top directors but no job
tested the nascent shop's mettle quite like this one. With an international
team working in multiple global locations, the logistics from the onset were
paramount.

"The scope of the job evolved throughout the project which created some unique
challenges," noted Dashing EP Danielle Lyons. "But, since we're a malleable
studio we were able to exceed even our own expectations."

With only a week to complete intensive, photo-realistic CG, Moggach and his
team worked around the clock to seamlessly fill out and frame wide shots of
the crowd, arena and ring in the spot's second half. The project's CG needs
included a chain-link fence, arena and thousands of cheering digital crowd.

"Dashing's model is about giving a streamlined team of expert craftsmen the
best tools available," commented Moggach. "The idea that a compact well-
equipped team of real experts can accomplish as much or more than a larger
less-equipped one was put to the test and proven viable."

Anchored by Founder and Creative Director Robert Moggach, Executive Producer
Danielle Lyons and a select team of top digital artists and technicians,
Dashing is tailor made for handling projects with demanding creative or
technical challenges and adapting to service emerging complex, cross-platform
creative needs.